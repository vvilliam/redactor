//
//  RDPDFView.m
//  Redactor
//
//  Created by Brian on 6/6/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "RDMenuAppendingPDFView.h"

@interface RDMenuAppendingPDFView ()
@property (nonatomic, assign) NSPoint isTrackingDragTouch;
@end

@implementation RDMenuAppendingPDFView

- (NSMenu *)menuForEvent:(NSEvent *)theEvent {
    return [RDMenuAppendingPDFView menu:[super menuForEvent:theEvent] withAppendedItems:self.menuItems];
}

+ (NSMenu *)menu:(NSMenu *)menu withAppendedItems:(NSArray *)items {
    if (items.count > 0) {
        [menu insertItem:[NSMenuItem separatorItem] atIndex:0];
        for (NSMenuItem *item in items.reverseObjectEnumerator) {
            [menu insertItem:item atIndex:0];
        }
    }
    return menu;
}

- (void)mouseDown:(NSEvent *)theEvent {
    [super mouseDown:theEvent];
    self.isTrackingDragTouch = [theEvent locationInWindow];
}

- (void)mouseDragged:(NSEvent *)theEvent {
    [super mouseDragged:theEvent];
    // Does not get called.
    // http://www.cocoabuilder.com/archive/cocoa/191837-missing-mouse-events-in-pdfview.html
}

- (void)mouseUp:(NSEvent *)theEvent {
    [super mouseUp:theEvent];
    if (CGPointEqualToPoint(self.isTrackingDragTouch, [theEvent locationInWindow])) {
        if (theEvent.type == NSLeftMouseUp) {
            if (self.didReceiveEventLeftMouseUp) {
                self.didReceiveEventLeftMouseUp(theEvent);
            }
        }
    }
}

- (void)PDFViewWillClickOnLink:(PDFView *)sender withURL:(NSURL *)url {
    // Overriden to prevent links from opening. No implementation needed.
}

@end
