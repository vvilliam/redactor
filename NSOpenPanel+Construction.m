//
//  NSOpenPanel+Construction.m
//  
//
//  Created by Brian on 6/14/15.
//
//

#import "NSOpenPanel+Construction.h"

@implementation NSOpenPanel (Construction)

+ (NSOpenPanel *)PDFOpenPanel {
    NSOpenPanel *openPanel = [[NSOpenPanel alloc] init];
    [openPanel canChooseFiles];
    [openPanel setAllowedFileTypes:[NSArray arrayWithObject:@"pdf"]];
    return openPanel;
}

@end
