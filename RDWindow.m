//
//  RDWindow.m
//  Redactor
//
//  Created by Brian on 5/31/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

@import Quartz;

#import "RDWindow.h"
#import "Document.h"
#import "PDFView+Annotation.h"
#import "MBProgressHUD.h"
#import "TesseractObject.h"
#import "RDAnnotation.h"
#import "PdfSelections.h"
#import "MakeImageFromPdfPage.h"
#import "RDMenuAppendingPDFView.h"
#import "Document+Construction.h"

@interface RDWindow () <NSWindowDelegate>

@property (nonatomic) NSMutableArray *dateAnnotations;
@property (nonatomic, assign) BOOL isSelectedName;
@property (nonatomic, assign) BOOL isSelectedAddress;
@property (nonatomic, assign) BOOL isSelectedDate;
@property (nonatomic, assign) BOOL isSelectedPhone;
@property (nonatomic, assign) BOOL isSelectedSSN;
@property (nonatomic, assign) BOOL isSelectedLink;


@property (nonatomic, weak) IBOutlet NSView *view;
@property (nonatomic, weak) IBOutlet PDFThumbnailView *thumbnailView;
//@property (nonatomic, weak) IBOutlet RDMenuAppendingPDFView *PDFView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *thumbnailViewWidth;

- (IBAction)toggleThumbnailSidebar:(id)sender;
- (IBAction)didReceiveTextChangeFromSearchField:(NSSearchField *)field;

@end

@implementation RDWindow

- (instancetype)initWithNibNamed:(NSString *)nibName {
    NSArray *arrayOfViews;
    [[NSBundle mainBundle] loadNibNamed:nibName owner:self topLevelObjects:&arrayOfViews];
    for (id object in arrayOfViews) {
        if ([object isKindOfClass:[RDWindow class]]) {
            self = object;
            self.delegate = self;
            return self;
        }
    }
    return nil;
}

- (instancetype)initWithContentRect:(NSRect)contentRect styleMask:(NSUInteger)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag {
    self = [super initWithContentRect:contentRect styleMask:aStyle backing:bufferingType defer:flag];
    if (self) {
        self.delegate = self;
    }
    return self;
}

- (instancetype)initWithContentRect:(NSRect)contentRect styleMask:(NSUInteger)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag screen:(NSScreen *)screen {
    self = [super initWithContentRect:contentRect styleMask:aStyle backing:bufferingType defer:flag screen:screen];
    if (self) {
        self.delegate = self;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.thumbnailView.PDFView = self.PDFView;
}


- (void)setDocument:(Document *)document {
    if (_document != document) {
        [self willSetDocument:document];
        _document = document;
        _PDFView.document = document.PDFDocument();
        [self didSetDocument:document];
    }
}

- (void)willSetDocument:(Document *)document {
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    self.isSelectedName = NO;
    if (document.title) {
        // Will crash if set to nil.
        self.title = document.title;
    }
}

- (void)didSetDocument:(Document *)document {
    if (!document.orderedTesseract) {
        [self.PDFView removeAllAnnotations];
    }
    if (document.ocred) {
        _PDFView.document = [[PDFDocument alloc]initWithData:[RDWindow searchablePDF:document]];
        [RDWindow buildAnnotations:document withView:self.PDFView];
    }
    
}

- (void)beginAnalyzingState:(NSString *)hudLabel {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.PDFView animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = hudLabel;
}

- (void)finishAnalyzingState {
    [MBProgressHUD hideHUDForView:self.PDFView animated:YES];
}

- (void)beginExportingState {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.PDFView animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Exporting...";
}

- (void)finishExportingState {
    [MBProgressHUD hideHUDForView:self.PDFView animated:YES];
}

- (IBAction)toggleThumbnailSidebar:(id)sender {
    [NSAnimationContext runAnimationGroup:^(NSAnimationContext *context) {
        context.duration = .25;
        self.thumbnailViewWidth.animator.constant = self.thumbnailViewWidth.animator.constant == 0.0 ? -self.thumbnailView.frame.size.width : 0.0;
    } completionHandler:nil];
}

- (IBAction)removeSearch:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"removeSearch" object:nil];
}

- (IBAction)saveSheet:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"saveSheet" object:nil];
}

- (void)redactSelection {
    // Needs implementation.
}

- (void)redactCurrentPage {
    [self.PDFView redactCurrentPage];
    self.PDFView.currentSelection = nil;
}

- (void)redactHighlights {
    [self.PDFView redactHighlights];
    self.PDFView.currentSelection = nil;
}

#pragma mark - Alternative function for PDF documents

- (void)showNames {
    [self.PDFView redactNamesFromDocument:self.document];
}

- (void)removeNames {
    [self.PDFView unredactNamesFromDocument:self.document];
}


- (void)showDates {
    [self.PDFView redactDatesFromDocument:self.document];
}

- (void)removeDates {
    [self.PDFView unredactDatesFromDocument:self.document];
}


- (void)showAddresses {
    [self.PDFView redactAddressesFromDocument:self.document];
}

- (void)removeAddresses {
    [self.PDFView unredactAddressesFromDocument:self.document];
}

- (void)showPhones {
    [self.PDFView redactPhonesFromDocument:self.document];
}

- (void)removePhones {
    [self.PDFView unredactPhonesFromDocument:self.document];
}

- (void)showSSNs {
    [self.PDFView redactSSNsFromDocument:self.document];
}

- (void)removeSSNs {
    [self.PDFView unredactSSNsFromDocument:self.document];
}

- (void)showLinks {
    [self.PDFView redactLinksFromDocument:self.document];
}

- (void)removeLinks {
    [self.PDFView unredactLinksFromDocument:self.document];
}



#pragma mark

- (void)setThumbnailView:(PDFThumbnailView *)thumbnailView {
    if (_thumbnailView != thumbnailView) {
        _thumbnailView = thumbnailView;
        _thumbnailView.backgroundColor = [NSColor colorWithCalibratedRed:0.79 green:0.84 blue:0.93 alpha:1.0];
        _thumbnailView.thumbnailSize = NSMakeSize(85, 110);
    }
}

- (void)setPDFView:(RDMenuAppendingPDFView *)PDFView {
    if (_PDFView != PDFView) {
        _PDFView = PDFView;
        _PDFView.enableDataDetectors = NO;
        _PDFView.backgroundColor = [NSColor colorWithCalibratedRed:0.85 green:0.85 blue:0.85 alpha:1.0];
        _PDFView.acceptsTouchEvents = YES;
        if(!self.document.ocred) {
        _PDFView.menuItems = @[
            [RDWindow menuItemRedactSelection],
            [RDWindow menuItemRedactAllInstancesOfWord],
            [RDWindow menuItemUnredactAllInstancesOfWord],
            [RDWindow menuItemRedactPage]
        ];
        }else {
            _PDFView.menuItems = @[
            [RDWindow menuItemRedactSelection],
//            [RDWindow menuItemRedactAllInstancesOfWord],
//            [RDWindow menuItemUnredactAllInstancesOfWord],
            [RDWindow menuItemRedactPage]
            ];
        }
        __weak RDWindow *weakself = self;
        _PDFView.didReceiveEventLeftMouseUp = ^(NSEvent *event) {
            [weakself annotateWithEvent:event];
        };
    }
}

+ (NSString *)annoLabel:(BOOL)selected {
    return selected ? @"name" : @"blue";
}

- (PDFAnnotationButtonWidget *)annotation:(NSString *)withFieldName andLocation:(NSRect)rect {
    PDFAnnotationButtonWidget *annotation = [[PDFAnnotationButtonWidget alloc] initWithBounds:rect];
    annotation.backgroundColor = [NSColor clearColor];
    annotation.color = [NSColor clearColor];
    annotation.fieldName = withFieldName;
    annotation.controlType = kPDFWidgetPushButtonControl;
    return annotation;
}

- (void)newAnnotes:(PDFSelection *)selection andCurrentPage:(PDFPage *)currentPage {
  
    [self.PDFView goToPage:currentPage];
    PDFAnnotationButtonWidget *annotation = [self annotation:[RDWindow annoLabel:YES] andLocation:[selection boundsForPage:[self.PDFView currentPage]]];
    
    if ((600 > annotation.bounds.size.height)) {
        [[self.PDFView currentPage] addAnnotation:annotation];
        [self toggleAnnotationButtonWidget:annotation];
        [self.PDFView setNeedsDisplay: YES];
    }
}

- (void)highlightWordsMatchingSearchTerm:(NSString *)searchTerm {
    self.PDFView.highlightedSelections = [RDWindow highlightedPDFSelectionsForQuery:searchTerm inDocument:self.PDFView.document];
    //this jumps to the first page with a search term on it
    if (self.PDFView.highlightedSelections.count) {
        NSArray *pages = [self.PDFView.highlightedSelections[0] pages];
        
        [self.PDFView goToPage:pages[0]];
    }
}

- (void)redactAllFromRightClick {
    [self.PDFView redactSelectedWordsForDocument:self.document];
    self.PDFView.currentSelection = nil;
}

- (void)unredactAllFromRightClick {
    [self.PDFView unredactSelectedWordsForDocument:self.document];
    self.PDFView.currentSelection = nil;
}

- (void)touchesBeganWithEvent:(NSEvent *)event {
    
}

- (void)annotateWithEvent:(NSEvent *)event {
    PDFAnnotationButtonWidget *newActiveAnnotation = NULL;
    NSPoint mousePoint = [self.PDFView convertPoint:[event locationInWindow] fromView:NULL];
    PDFPage *activePage = [self activePage:mousePoint];
    NSPoint pagePoint = [self.PDFView convertPoint:mousePoint toPage:activePage];
    
    newActiveAnnotation = [RDWindow annotationWithWindowPoint:[self.PDFView convertPoint:event.locationInWindow fromView:NULL] inPDFView:self.PDFView];
    
    if ([newActiveAnnotation.fieldName isEqualToString:@"line"]) {
        [activePage removeAnnotation:newActiveAnnotation];
        [self.PDFView setNeedsDisplay: YES];
    }
    [self toggleAnnotationButtonWidget:newActiveAnnotation];
    
    if (newActiveAnnotation == NULL && !self.document.ocred){
        if(!([[PdfSelections newSelection:activePage atThePoint:pagePoint].string rangeOfCharacterFromSet:[RDWindow characterSets]].location > 1)){
            [self newAnnotes:[PdfSelections newSelection:activePage atThePoint:pagePoint] andCurrentPage:activePage];
        }
    }
    else if (!(newActiveAnnotation == NULL) && !(self.document.ocred)) {
        [activePage removeAnnotation:newActiveAnnotation];
    }
//    if([self.PDFView areaOfInterestForPoint:pagePoint] >50 && newActiveAnnotation == NULL){
//        NSLog(@"image maybe? %ld",(long)[self.PDFView areaOfInterestForPoint:pagePoint]);
//        [self newAnnotes:[PdfSelections newSelection:activePage atThePoint:pagePoint] andCurrentPage:activePage];
//    }

    
}

- (PDFPage *)activePage:(NSPoint)atPoint {
    return [self.PDFView pageForPoint:atPoint nearest:YES];
}

- (void)toggleAnnotationButtonWidget:(PDFAnnotationButtonWidget *)widget {

    
    if (widget.backgroundColor.alphaComponent > 0){
        widget.backgroundColor = [NSColor clearColor];
    } else {
        widget.backgroundColor = [PdfSelections theOpaqueColor];
    }
    if ([widget.fieldName isEqualToString:@"page"]) {
        [self.PDFView.currentPage removeAnnotation:widget];
    }
    [self.PDFView setNeedsDisplay: YES];
}

- (void)toggleNameHighlights {
    self.isSelectedName = !self.isSelectedName;
    
    if (self.document.ocred) {
        if (self.isSelectedName) {
            [self highlightNamesWithColor:[PdfSelections theOpaqueColor]];
        } else {
            [self highlightNamesWithColor:[NSColor clearColor]];
        }
    } else {
        if (self.isSelectedName){
            [self showNames];
        } else {
            [self removeNames];
        }
    }
    [self.PDFView goToPage:self.PDFView.currentPage];
    [self.PDFView setNeedsDisplay:YES];
}

- (void)toggleDateHighlights {
    self.isSelectedDate = !self.isSelectedDate;
    
    if (self.document.ocred) {
        if (self.isSelectedDate) {
            [self highlightDateWithColor:[PdfSelections theOpaqueColor]];
        } else {
            [self highlightDateWithColor:[NSColor clearColor]];
        }
    } else {
        if (self.isSelectedDate){
            [self showDates];
        } else {
            [self removeDates];
        }
    }
    [self.PDFView goToPage:self.PDFView.currentPage];
    [self.PDFView setNeedsDisplay:YES];
}

- (void)toggleAddressHighlights {
    self.isSelectedAddress = !self.isSelectedAddress;
    
    if (self.document.ocred) {
        if (self.isSelectedAddress) {
            [self highlightAddressWithColor:[PdfSelections theOpaqueColor]];
        } else {
            [self highlightAddressWithColor:[NSColor clearColor]];
        }
    } else {
        if (self.isSelectedAddress){
            [self showAddresses];
        } else {
            [self removeAddresses];
    }
        }
    [self.PDFView goToPage:self.PDFView.currentPage];
    [self.PDFView setNeedsDisplay:YES];
}

- (void)togglePhoneHighlights {
    self.isSelectedPhone = !self.isSelectedPhone;
    
    if (self.document.ocred) {
        if (self.isSelectedPhone) {
            [self highlightPhoneWithColor:[PdfSelections theOpaqueColor]];
        } else {
            [self highlightPhoneWithColor:[NSColor clearColor]];
        }
    } else {
        if (self.isSelectedPhone){
            [self showPhones];
        } else {
            [self removePhones];
        }
    }
    [self.PDFView goToPage:self.PDFView.currentPage];
    [self.PDFView setNeedsDisplay:YES];
}

- (void)toggleSSNHighlights {
    self.isSelectedSSN = !self.isSelectedSSN;
    
    if (self.document.ocred) {
        if (self.isSelectedSSN) {
            [self highlightSSNWithColor:[PdfSelections theOpaqueColor]];
        } else {
            [self highlightSSNWithColor:[NSColor clearColor]];
        }
    } else {
        if (self.isSelectedSSN){
            [self showSSNs];
        } else {
            [self removeSSNs];
        }
    }
    [self.PDFView goToPage:self.PDFView.currentPage];
    [self.PDFView setNeedsDisplay:YES];
}

- (void)toggleLinkHighlights {
    self.isSelectedLink = !self.isSelectedLink;
    
    if (self.document.ocred) {
        if (self.isSelectedLink) {
            [self highlightLinkWithColor:[PdfSelections theOpaqueColor]];
        } else {
            [self highlightLinkWithColor:[NSColor clearColor]];
        }
    } else {
        if (self.isSelectedLink){
            [self showLinks];
        } else {
            [self removeLinks];
        }
    }
    [self.PDFView goToPage:self.PDFView.currentPage];
    [self.PDFView setNeedsDisplay:YES];
}

- (void)highlightNamesWithColor:(NSColor *)color {
    for (PDFAnnotationButtonWidget *widget in self.PDFView.nameWidgets) {
        widget.backgroundColor = color;
    }
}

- (void)highlightAddressWithColor:(NSColor *)color {
    for (PDFAnnotationButtonWidget *widget in self.PDFView.addressWidgets) {
        widget.backgroundColor = color;
    }
}

- (void)highlightPhoneWithColor:(NSColor *)color {
    for (PDFAnnotationButtonWidget *widget in self.PDFView.phoneWidgets) {
        widget.backgroundColor = color;
    }
}

- (void)highlightDateWithColor:(NSColor *)color {
    for (PDFAnnotationButtonWidget *widget in self.PDFView.dateWidgets) {
        widget.backgroundColor = color;
    }
}

- (void)highlightLinkWithColor:(NSColor *)color {
    for (PDFAnnotationButtonWidget *widget in self.PDFView.linkWidgets) {
        widget.backgroundColor = color;
    }
}

- (void)highlightSSNWithColor:(NSColor *)color {
    for (PDFAnnotationButtonWidget *widget in self.PDFView.ssnWidgets) {
        widget.backgroundColor = color;
    }
}


- (void)finalizeRedactions {
    for (PDFAnnotationButtonWidget *widget in [RDWindow annotationWidgetsForPDFDocument:self.PDFView.document]) {
        if ([widget.type isEqual:@"Widget"]) {
            if (widget.backgroundColor.alphaComponent > 0) {
                widget.backgroundColor = [NSColor blackColor];
            }
        }
    }
    [self.PDFView setNeedsDisplay:YES];
}

+ (PDFAnnotationButtonWidget *)annotationWithWindowPoint:(NSPoint)point inPDFView:(PDFView *)view {
    PDFPage *activePage = [view pageForPoint:point nearest:YES];
    NSPoint pagePoint = [view convertPoint:point toPage:activePage];
    for (PDFAnnotation *annotation in activePage.annotations) {
        if (NSPointInRect(pagePoint, annotation.bounds)) {
            if ([annotation isKindOfClass:[PDFAnnotationButtonWidget class]]) {
                return (PDFAnnotationButtonWidget *)annotation;
            }
        }
    }
    return nil;
}

//+ (void)buildAnnotations:(Document *)document withView:(PDFView *)PDFView {
//    PDFDocument *PDFDocument = document.PDFDocument();
//    for (NSInteger pageCount = 0; pageCount < PDFDocument.pageCount; pageCount++) {
//        Page *page = [document pageAtIndex:pageCount];
//        for (NSInteger index = 0; index < page.words.count; index++) {
//            PDFAnnotationButtonWidget *widget =
//            [RDWindow newAnnotationWithPDFPage:[PDFDocument pageAtIndex:pageCount]
//                                          word:[document pageAtIndex:pageCount].words[index]
//                                       andView:PDFView];
//            [PDFView.currentPage addAnnotation:widget];
//            [PDFView setNeedsDisplay:YES];
//            
//        }
//    }
//    [PDFView goToFirstPage:@"self"];
//}

+ (void)buildAnnotations:(Document *)document withView:(PDFView *)PDFView {
    [PDFView goToFirstPage:@"self"];
    PDFDocument *PDFDocument = document.PDFDocument();
    for (NSInteger pageCount = 0; pageCount < PDFDocument.pageCount; pageCount++) {
        Page *page = [document pageAtIndex:pageCount];
        for (NSInteger index = 0; index < page.words.count; index++) {
            PDFAnnotationButtonWidget *widget =
            [RDWindow newAnnotationWithPDFPage:[PDFDocument pageAtIndex:pageCount]
                                          word:[document pageAtIndex:pageCount].words[index]
                                       andView:PDFView];
            [PDFView.currentPage addAnnotation:widget];
            [PDFView setNeedsDisplay:YES];
        }
        [PDFView goToNextPage:@"self"];
    }
    [PDFView goToFirstPage:@"self"];
}


+ (PDFAnnotationButtonWidget *)newAnnotationWithPDFPage:(PDFPage *)pageNumber word:(Word *)word andView:(PDFView *)view {
//    [view goToPage:pageNumber];
    return [PDFView annotation:word.typeOfWord andLocation:word.coordinate];
}

+ (NSArray *)annotationWidgetsForPDFDocument:(PDFDocument *)document {
    NSMutableArray *widgets = [[NSMutableArray alloc] init];
    for (NSUInteger index = 0; index < document.pageCount; index++) {
        [widgets addObjectsFromArray:[document pageAtIndex:index].annotations];
    }
    return widgets;
}

+ (PDFPage *)pageFromImage:(NSImage *)image {
    return [[PDFPage alloc]initWithImage:image];
}

+ (NSData *)imageDataForPage:(PDFPage *)page {
    CGSize size = CGPDFPageGetBoxRect(page.pageRef, kCGPDFCropBox).size;
    NSImage *image = [MakeImageFromPdfPage blankImageWithSize:CGSizeMake(size.width, size.height)];
    for (PDFAnnotation *annotation in page.annotations) {
        [image lockFocus];
        [[MakeImageFromPdfPage blackImageWithSize:annotation.bounds.size] drawInRect:annotation.bounds];
        [image unlockFocus];
    }
    return image.TIFFRepresentation;
}

+ (void)removeAllAnnotationsFromDocument:(PDFDocument *)document {
    for (NSInteger index = 0; index < document.pageCount; index++) {
        PDFPage *activePage = [document pageAtIndex:index];
        for (NSInteger index = 0; index < activePage.annotations.count; index++) {
            [activePage removeAnnotation:activePage.annotations[index]];
        }
    }
}

+ (NSMenuItem *)menuItemRedactSelection {
    return [[NSMenuItem alloc]initWithTitle:@"Redact" action:@selector(redactHighlights) keyEquivalent:@""];
}

+ (NSMenuItem *)menuItemRedactPage {
    return [[NSMenuItem alloc]initWithTitle:@"Redact Page" action:@selector(redactCurrentPage) keyEquivalent:@""];
}

+ (NSMenuItem *)menuItemUnredactAllInstancesOfWord {
    return [[NSMenuItem alloc]initWithTitle:@"Unredact All Instances" action:@selector(unredactAllFromRightClick) keyEquivalent:@""];
}

+ (NSMenuItem *)menuItemRedactAllInstancesOfWord {
    return [[NSMenuItem alloc]initWithTitle:@"Redact All Instances" action:@selector(redactAllFromRightClick) keyEquivalent:@""];
}

+ (PDFDocument *)nonsearchablePDFFromDocument:(PDFDocument *)document {
    PDFDocument *pdf = [[PDFDocument alloc] init];
    for (NSUInteger index = 0; index < document.pageCount; index++) {
        PDFPage *page = [document pageAtIndex:index];
        NSImage *rasterizedPage = [self imageFromPage:page];
        PDFPage *newPage = [RDWindow pageFromImage:rasterizedPage];
        [pdf insertPage:newPage atIndex:index];
    };
    return pdf;
}

+ (NSImage *)imageFromPage:(PDFPage *)page {
    NSImage *image = [[NSImage alloc] initWithSize:CGPDFPageGetBoxRect(page.pageRef, kCGPDFCropBox).size];
    [image lockFocus];
    [page drawWithBox:kPDFDisplayBoxCropBox];
    [image unlockFocus];
    return image;
}

+ (NSArray *)highlightedPDFSelectionsForQuery:(NSString *)query inDocument:(PDFDocument *)document {
    NSArray *selections = [document findString:query withOptions:NSCaseInsensitiveSearch];
    for (NSInteger index = 0; index < selections.count; index++) {
        [selections[index] setColor:[NSColor yellowColor]];
    }
    return selections;
}

+ (NSAlert *)alertForOCRConfirmation {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"OK"];
    [alert addButtonWithTitle:@"Cancel"];
    [alert setMessageText:@"This document must be OCR'd first?"];
    [alert setInformativeText:@"Press Okay to convert image to text."];
    [alert setAlertStyle:NSWarningAlertStyle];
    return alert;
}

+ (NSCharacterSet *)characterSets {
    NSCharacterSet *charactersToCheck = [NSCharacterSet characterSetWithCharactersInString:@"\"abcdefghijk§lmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123+4567890_.,*&@#$%'?!-)([]:/|"];
    return charactersToCheck;
}

- (IBAction)didReceiveTextChangeFromSearchField:(NSSearchField *)field {
    [self highlightWordsMatchingSearchTerm:field.stringValue];
}

- (BOOL)windowShouldClose:(RDWindow *)window {
    [[RDWindow alertViewWithCloseConfirmation] beginSheetModalForWindow:window completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == -NSModalResponseContinue) {
            [window close];
        } else if (returnCode == -NSModalResponseStop) {
            [self savePDFWithCompletionHandler:^(BOOL success){
                if (success) {
                    [window close];
                }
            }];
        }
    }];
    return NO;
}

- (void)closeWithCompletionHandler:(void (^)(void))handler {
    [[RDWindow alertViewWithCloseConfirmation] beginSheetModalForWindow:self completionHandler:^(NSModalResponse returnCode) {
        if (returnCode == -NSModalResponseContinue) {
            [self close];
            handler();
        } else if (returnCode == -NSModalResponseStop) {
            [self savePDFWithCompletionHandler:^(BOOL success){
                if (success) {
                    [self close];
                    handler();
                }
            }];
        }
    }];
}

- (void)close {
    if (self.willCloseHandler) {
        self.willCloseHandler();
    }
    [super close];
}

+ (NSAlert *)alertViewWithCloseConfirmation {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"Save"];
    [alert addButtonWithTitle:@"Cancel"];
    [alert addButtonWithTitle:@"Don't save"];
    [alert setMessageText:@"Do you want to save your changes?"];
    [alert setInformativeText:@"Your changes will be lost if you don't save them."];
    [alert setAlertStyle:NSWarningAlertStyle];
    return alert;
}

- (void)savePDFWithCompletionHandler:(void (^)(BOOL success))handler {
    if (self.document.ocred) {
        [self saveWindow:self withCompletionHandler:handler];
    } else {
        if ([[RDWindow alert] runModal] == NSAlertFirstButtonReturn) {
            NSSavePanel *savePanel = [RDWindow newPDFSavePanel];
            if ([savePanel runModal] == YES) {
                [self beginExportingState];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [self.document.searchablePDF writeToURL:savePanel.URL atomically:YES];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self finishExportingState];
                        if (handler) {
                            handler(YES);
                        }
                    });
                });
            } else {
                handler(NO);
            }
        } else {
            [self saveWindow:self withCompletionHandler:handler];
        }
    }
}

- (void)saveWindow:(RDWindow *)window withCompletionHandler:(void (^)(BOOL success))handler {
    [window finalizeRedactions];
    NSSavePanel *savePanel = [RDWindow newPDFSavePanel];
    if ([savePanel runModal] == YES) {
        [window beginExportingState];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[RDWindow nonsearchablePDFFromDocument:window.document.PDFDocument()] writeToURL:savePanel.URL];
            dispatch_async(dispatch_get_main_queue(), ^{
                [window finishExportingState];
                if (handler) {
                    handler(YES);
                }
            });
        });
    } else {
        handler(NO);
    }
}

+ (NSSavePanel *)newPDFSavePanel {
    NSSavePanel *savePanel = [[NSSavePanel alloc] init];
    [savePanel setAllowedFileTypes:[NSArray arrayWithObject:@"pdf"]];
    return savePanel;
}

+ (NSAlert *)alert {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"Searchable PDF"];
    [alert addButtonWithTitle:@"Non-Searchable PDF"];
    [alert setMessageText:@"Create PDF"];
    [alert setInformativeText:@"Searchable PDF is in beta.  Searchable PDF does not support tables, images, and colors."];
    [alert setAlertStyle:NSInformationalAlertStyle];
    return alert;
}

- (IBAction)didSelectToggleZoomFromSegmentedCell:(NSSegmentedCell *)cell {
    if (cell.selectedSegment == 1) {
        [self.PDFView zoomIn:self];
    } else {
        [self.PDFView zoomOut:self];
    }
}


+ (NSData *)searchablePDF:(Document *)document {
    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((CFDataRef)nil);
    CGPDFDocumentRef documentRef = CGPDFDocumentCreateWithProvider(dataProvider);
    CFMutableDataRef mutableData = CFDataCreateMutable(NULL, 0);
    CGDataConsumerRef dataConsumer = CGDataConsumerCreateWithCFData(mutableData);
    CGPDFPageRef page = CGPDFDocumentGetPage(documentRef, 1);
    CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
    CGContextRef pdfContext = CGPDFContextCreate(dataConsumer, &pageRect, NULL);
    CFRelease(dataConsumer);

    PDFDocument *PDFDocument = document.PDFDocument();
    
    PDFPage *pageXX = [PDFDocument pageAtIndex:0];
    CGSize size = CGPDFPageGetBoxRect(pageXX.pageRef, kCGPDFCropBox).size;
    
    for (NSInteger pageCount = 0; pageCount < PDFDocument.pageCount; pageCount++) {
        Page *page = [document pageAtIndex:pageCount];
        
        NSData *pageWithBlackLines = [page.pageImage TIFFRepresentation];
        NSImage *image = page.pageImage;
        
        CGImageSourceRef source = CGImageSourceCreateWithData((CFDataRef)pageWithBlackLines, NULL);
        CGRect mediaRect = CGRectMake(0, 0, size.width, size.height);
        CGContextBeginPage(pdfContext, &mediaRect);
        CGImageRef imageRef = CGImageSourceCreateImageAtIndex(source, 0, NULL);
        CFRelease(source);
        /// may need to be adjusted for retina/nonretina screens
        CGContextDrawImage(pdfContext, CGRectMake(0, 0, image.size.width/2, image.size.height/2), imageRef);
        CFRelease(imageRef);

        for (NSInteger index = 0; index < page.words.count; index++) {
            NSString *string = [[document pageAtIndex:pageCount].words[index] text];
            CGSize size = CGSizeMake([[document pageAtIndex:pageCount].words[index] coordinate].size.width, [[document pageAtIndex:pageCount].words[index] coordinate].size.height);
            
            NSAttributedString *hiddenStringForSearching = [[NSAttributedString alloc] initWithString:string
                        attributes:@{
                                    NSForegroundColorAttributeName : [NSColor clearColor],
                                    NSStrokeColorAttributeName : [NSColor colorWithCalibratedRed:0.85 green:0.85 blue:0.85 alpha:0.0],
                                    NSFontAttributeName : [NSFont fontWithName:@"Times" size:size.height],
                                    NSKernAttributeName : [NSNumber numberWithFloat:1],
                                    }];
            NSRect sampleRect = [[document pageAtIndex:pageCount].words[index] coordinate];
            
//            CGContextSaveGState(pdfContext);
            
            CTLineRef displayLine = CTLineCreateWithAttributedString((CFAttributedStringRef)hiddenStringForSearching);

            CGContextSetFillColorWithColor(pdfContext, [NSColor clearColor].CGColor);
            CGFloat clear[4] = {0, 0,
                0, 0};
            CGContextSetStrokeColor(pdfContext, clear);
            
            CGContextSetFillColorWithColor(pdfContext, [NSColor clearColor].CGColor);

            CGContextClipToRect(pdfContext, NSMakeRect(sampleRect.origin.x, sampleRect.origin.y, sampleRect.size.width, sampleRect.size.height));

            CGContextSetTextPosition(pdfContext, sampleRect.origin.x, sampleRect.origin.y+3);
            CTLineDraw(displayLine, pdfContext);
            CFRelease(displayLine );
//            CGContextRestoreGState(pdfContext);

        }

        CGContextEndPage(pdfContext);
    }
    
    NSData *data = (__bridge NSData *)(mutableData);
    CFRelease(pdfContext);
    CFRelease(mutableData);
    return data;
}



@end
