//
//  PDFPage+ImageConversion.m
//  
//
//  Created by Brian on 6/14/15.
//
//

#import "PDFPage+ImageConversion.h"

@implementation PDFPage (ImageConversion)

+ (NSImage *)imageFromPDFPage:(PDFPage *)page {
    NSImage *image = [[NSImage alloc] initWithSize:CGPDFPageGetBoxRect(page.pageRef, kCGPDFCropBox).size];
    [image lockFocus];
    [page drawWithBox:kPDFDisplayBoxCropBox];
    [image unlockFocus];
    return image;
}

@end
