//
//  PDFDocument+Validation.h
//  
//
//  Created by Brian on 6/13/15.
//
//

@import Quartz;

@interface PDFDocument (Validation)

- (BOOL)isText;

@end
