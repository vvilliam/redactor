//
//  RDWindow.h
//  Redactor
//
//  Created by Brian on 5/31/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

@import Cocoa;

#import "Document.h"
#import "RDMenuAppendingPDFView.h"

@interface RDWindow : NSWindow

@property (nonatomic, weak) IBOutlet RDMenuAppendingPDFView *PDFView;

@property (nonatomic) Document *document;
@property (nonatomic, copy) void (^willCloseHandler)(void);

- (instancetype)initWithNibNamed:(NSString *)nibName;
- (void)beginAnalyzingState:(NSString *)hudLabel;
- (void)finishAnalyzingState;
- (void)beginExportingState;
- (void)finishExportingState;
- (void)finalizeRedactions;
- (void)closeWithCompletionHandler:(void (^)(void))handler;
- (void)redactHighlights;
- (void)redactCurrentPage;
- (void)toggleNameHighlights;
- (void)toggleDateHighlights;
- (void)toggleAddressHighlights;
- (void)togglePhoneHighlights;
- (void)toggleSSNHighlights;
- (void)toggleLinkHighlights;


// test
//- (BOOL)isPossiblySearchable;
@end
