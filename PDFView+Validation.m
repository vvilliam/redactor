//
//  PDFView+Validation.m
//  
//
//  Created by Brian on 6/15/15.
//
//

#import "PDFView+Validation.h"
#import "RDEventWithMutableLocation.h"

@implementation PDFView (Validation)

- (BOOL)isPossiblySearchable {

    
    for (NSInteger xPoint = 0; xPoint < 900; xPoint++) {
        for (NSInteger yPoint = 0; yPoint < 792; yPoint++) {
            if ([self areaOfInterestForMouse:[PDFView newEventWithLocation:CGPointMake(xPoint, yPoint)]] > 10) {
                return YES;
            }
        }
    }
    return NO;
}

+ (NSEvent *)newEventWithLocation:(CGPoint)location {
    return [[RDEventWithMutableLocation alloc] initWithWindowLocation: NSPointFromCGPoint(location)];
}

@end
