//
//  RDPDFView.h
//  Redactor
//
//  Created by Brian on 6/6/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

@import Quartz;

@interface RDMenuAppendingPDFView : PDFView

@property (nonatomic, copy) NSArray *menuItems;
@property (nonatomic, copy) void (^didReceiveEventLeftMouseUp)(NSEvent *event);

@end
