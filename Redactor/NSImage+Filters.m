//
//  NSImage+Filters.m
//  Tesseract OCR Mac
//
//  Converted by Harry Shamansky on 1/22/15
//  Original Copyright (c) 2014 Daniele Galiotto - www.g8production.com. All rights reserved.
//

#import "NSImage+Filters.h"
#import <QuartzCore/CoreImage.h>

@implementation NSImage (Filters)

- (NSImage *)blackAndWhite
{
    CIImage *beginImage = [CIImage imageWithData:self.TIFFRepresentation];

    CIImage *blackAndWhite = [CIFilter filterWithName:@"CIColorControls" keysAndValues:kCIInputImageKey, beginImage, @"inputBrightness", @0.0, @"inputContrast", @1.1, @"inputSaturation", @0.0, nil].outputImage;
    CIImage *output = [CIFilter filterWithName:@"CIExposureAdjust" keysAndValues:kCIInputImageKey, blackAndWhite, @"inputEV", @0.7, nil].outputImage;
    
    NSCIImageRep *rep = [NSCIImageRep imageRepWithCIImage:output];
    NSImage *newImage = [[NSImage alloc] initWithSize:rep.size];
    [newImage addRepresentation:rep];

    return newImage;
}

typedef enum {
    ALPHA = 0,
    BLUE = 1,
    GREEN = 2,
    RED = 3
} PIXELS;

- (NSImage *)grayScale
{
    CGSize size = [self size];
    int width = size.width;
    int height = size.height;
    
    // the pixels will be painted to this array
    uint32_t *pixels = (uint32_t *) malloc(width * height * sizeof(uint32_t));
    
    // clear the pixels so any transparency is preserved
    memset(pixels, 0, width * height * sizeof(uint32_t));
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // create a context with RGBA pixels
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * sizeof(uint32_t), colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    
    // paint the bitmap to our context which will fill in the pixels array
    CGImageSourceRef source;
    source = CGImageSourceCreateWithData((CFDataRef)[self TIFFRepresentation], NULL);
    CGImageRef maskRef = CGImageSourceCreateImageAtIndex(source, 0, NULL);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), maskRef);
    
    
    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            uint8_t *rgbaPixel = (uint8_t *) &pixels[y * width + x];
            
            // convert to grayscale using recommended method: http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
            uint32_t gray = 0.3 * rgbaPixel[RED] + 0.59 * rgbaPixel[GREEN] + 0.11 * rgbaPixel[BLUE];
            
            // set the pixels to gray
            rgbaPixel[RED] = gray;
            rgbaPixel[GREEN] = gray;
            rgbaPixel[BLUE] = gray;
        }
    }
    
    // create a new CGImageRef from our context with the modified pixels
    CGImageRef image = CGBitmapContextCreateImage(context);
    
    // we're done with the context, color space, and pixels
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    free(pixels);
    
    // make a new NSImage to return
    NSImage *newImage = [[NSImage alloc] initWithCGImage:image size:[self size]];

    // we're done with image now too
    CGImageRelease(image);
    CFRelease(source);
    CGImageRelease(maskRef);
    
    return newImage;
}

- (NSImage*)imageRotatedByDegrees:(CGFloat)degrees {
    // Calculate the bounds for the rotated image
    // We do this by affine-transforming the bounds rectangle
    NSRect imageBounds = {NSZeroPoint, [self size]};
    NSBezierPath* boundsPath = [NSBezierPath bezierPathWithRect:imageBounds];
    NSAffineTransform* transform = [NSAffineTransform transform];
    [transform rotateByDegrees:degrees];
    [boundsPath transformUsingAffineTransform:transform];
    NSRect rotatedBounds = {NSZeroPoint, [boundsPath bounds].size};
    NSImage* rotatedImage = [[NSImage alloc] initWithSize:rotatedBounds.size] ;
    
    // Center the image within the rotated bounds
    imageBounds.origin.x = NSMidX(rotatedBounds) - (NSWidth(imageBounds) / 2);
    imageBounds.origin.y = NSMidY(rotatedBounds) - (NSHeight(imageBounds) / 2);
    
    // Start a new transform, to transform the image
    transform = [NSAffineTransform transform];
    
    // Move coordinate system to the center
    // (since we want to rotate around the center)
    [transform translateXBy:+(NSWidth(rotatedBounds) / 2)
                        yBy:+(NSHeight(rotatedBounds) / 2)];
    // Do the rotation
    [transform rotateByDegrees:degrees];
    // Move coordinate system back to normal (bottom, left)
    [transform translateXBy:-(NSWidth(rotatedBounds) / 2)
                        yBy:-(NSHeight(rotatedBounds) / 2)];
    
    // Draw the original image, rotated, into the new image
    // Note: This "drawing" is done off-screen.
    [rotatedImage lockFocus];
    [transform concat];
    [self drawInRect:imageBounds fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0] ;
    [rotatedImage unlockFocus];
    
    return rotatedImage;
}

@end
