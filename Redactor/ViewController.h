//
//  ViewController.h
//  Redactor
//
//  Created by William Palin on 4/8/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Document.h"
#import "Tesseract.h"
#import <QuartzCore/QuartzCore.h>
#import <Quartz/Quartz.h>
#import "WindowController.h"

@interface ViewController : NSViewController <TesseractDelegate>
{
    PDFAnnotation	*_activeAnnotation;
    NSPoint			_mouseDownLoc;
    NSPoint			_clickDelta;
    NSRect			_wasBounds;
    BOOL			_mouseDownInAnnotation;
    BOOL			_dragging;
    BOOL			_resizing;
    BOOL			_editMode;
}

@property (nonatomic, assign) BOOL isSelected;


@end

