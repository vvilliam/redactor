//
//  PdfSelections.m
//  Redactor
//
//  Created by William Palin on 5/18/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "PdfSelections.h"
#import "Word.h"

@implementation PdfSelections


+ (PDFSelection *)newSelection:(PDFPage *)activePage atThePoint:(NSPoint)pagePoint {

    PDFSelection *sel = [activePage selectionForWordAtPoint:pagePoint];
    CGFloat one = [sel boundsForPage:activePage].size.height;
    [sel extendSelectionAtEnd:1];
    
    NSString *commaRight = sel.string;
    if ([commaRight hasSuffix:@","] || [commaRight hasSuffix:@"."] || [commaRight hasSuffix:@"'"]) {
        [sel extendSelectionAtEnd:1];
    }
    
    if ([commaRight isEqualToString:@". "]) {
        [sel extendSelectionAtEnd:1];
    }
    
    CGFloat two = [sel boundsForPage:activePage].size.height;
    if (two > one) {
        [sel extendSelectionAtEnd:-1];
    }
    
    
    return sel;
}

+ (NSColor *)theOpaqueColor {
    return [NSColor colorWithCalibratedWhite:0.000 alpha:0.440];
}

+ (NSPoint)averagePoint:(NSRect)rect{
    NSPoint newPoint = NSMakePoint(rect.origin.x + rect.size.width*0.5, rect.origin.y + rect.size.height*0.5);
    return newPoint;
}

+ (void)removeAnnotation:(Word *)word onPage:(PDFPage *)pageX{
    [pageX removeAnnotation:[pageX annotationAtPoint:[PdfSelections averagePoint:[[pageX selectionForRange:word.rangeForWords] boundsForPage:pageX]]]];
}

+ (void)drawAnnotation:(Word *)word onPage:(PDFPage *)pageX{
    [pageX addAnnotation:[PdfSelections nameAnnotation:[[PdfSelections getLength:[pageX selectionForWordAtPoint:[PdfSelections averagePoint:[[pageX selectionForRange:word.rangeForWords] boundsForPage:pageX]]] atPage:pageX] boundsForPage:pageX]]];
}

+ (PDFSelection *)getLength:(PDFSelection *)sel atPage:(PDFPage *)pageX {
    CGFloat one = [sel boundsForPage:pageX].size.height;
    [sel extendSelectionAtEnd:1];
    
    NSString *commaRight = sel.string;
    if ([commaRight hasSuffix:@","] || [commaRight hasSuffix:@"."] || [commaRight hasSuffix:@"'"]) {
        [sel extendSelectionAtEnd:1];
    }
    
    CGFloat two = [sel boundsForPage:pageX].size.height;
    if (two > one) {
        [sel extendSelectionAtEnd:-1];
    }
    return sel;
}

+ (PDFAnnotationButtonWidget *)nameAnnotation:(NSRect)rect{
    PDFAnnotationButtonWidget *annotation = [[PDFAnnotationButtonWidget alloc] initWithBounds:rect];
    annotation.backgroundColor = [PdfSelections theOpaqueColor];
    annotation.color = [NSColor clearColor];
    annotation.fieldName = @"name";
    annotation.controlType = kPDFWidgetPushButtonControl;
    return annotation;
}

+ (void)removeHiddenAnnotations:(PDFDocument *)document {
    for (NSInteger pageCount = 0; pageCount < [document pageCount]; pageCount++) {
        for (PDFAnnotationButtonWidget *anno in [[[document pageAtIndex:pageCount] annotations] reverseObjectEnumerator]) {
            if(anno.backgroundColor.alphaComponent == 0) {
                [anno setBackgroundColor:[NSColor clearColor]];
                [[document pageAtIndex:pageCount] removeAnnotation:anno];
            }
        }
    }
}

+ (void)cleanPdfDocument:(PDFDocument *)document {
    for (NSInteger index = 0; index < [document pageCount]; index++) {
        for (NSInteger index = 0; index < [[[document pageAtIndex:index] annotations] count]; index++) {
            [[document pageAtIndex:index] removeAnnotation:[[document pageAtIndex:index] annotations][index]];
        }
    }
}
@end
