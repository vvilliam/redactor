//
//  Line.h
//  RedactPro
//
//  Created by William Palin on 3/18/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Line : NSObject

@property (nonatomic, copy) NSArray *textPerLine; //array of document text broken down by line
@property (nonatomic, assign) NSArray *confidencePerLine; //array of document confidence for each line
@property (nonatomic, assign) NSRect cooridnate; // array of nsrects stored as strings for each line

@end
