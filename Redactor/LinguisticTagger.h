//
//  LinguisticTagger.h
//  RedactPro
//
//  Created by William Palin on 3/14/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Document.h"

@interface LinguisticTagger : NSObject

+ (NSMutableArray *)lastNames:(NSString *)strings;
+ (NSMutableArray *)phoneNumbers:(NSString *)strings;
+ (NSMutableArray *)addresses:(NSString *)string;
+ (NSMutableArray *)dates:(NSString *)string;
+ (NSMutableArray *)SSNs:(NSString *)string;
+ (NSMutableArray *)fullNames:(NSString *)string;

@end
