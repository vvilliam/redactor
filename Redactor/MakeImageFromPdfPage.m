//
//  MakeImageFromPdfPage.m
//  Redactor
//
//  Created by William Palin on 4/8/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "MakeImageFromPdfPage.h"
#import <Quartz/Quartz.h>
#import "NSImage+Filters.h"

@implementation MakeImageFromPdfPage



+(NSImage *)imageFromPDFData:(NSData *)PDFData {
    
        NSPDFImageRep* pdfImageRep = [NSPDFImageRep imageRepWithData:PDFData];
        CGFloat factor = 300/72;
        CGFloat retina = [[NSScreen mainScreen] backingScaleFactor];
        NSSize sizeX = NSMakeSize(pdfImageRep.bounds.size.width*retina, pdfImageRep.bounds.size.height*retina);

        NSImage* scaledImage = [NSImage imageWithSize:sizeX flipped:NO drawingHandler:^BOOL(NSRect dstRect){
            [pdfImageRep drawInRect:dstRect];
            return YES;
        }];
    
    
        NSImageRep* scaledImageRep = [[scaledImage representations] firstObject];
        
        scaledImageRep.pixelsWide = pdfImageRep.size.width * factor;
        scaledImageRep.pixelsHigh = pdfImageRep.size.height * factor;
        NSBitmapImageRep* pngImageRep = [NSBitmapImageRep imageRepWithData:[scaledImage TIFFRepresentation]];
        NSData* finalData = [pngImageRep representationUsingType:NSJPEGFileType properties:nil];
        
        NSImage *img = [[NSImage alloc] initWithData:finalData];
        
        return img;
}


+ (NSSize)giveMeSize:(NSString *)filePath {
    NSPDFImageRep* pdfImageRep = [NSPDFImageRep imageRepWithData:[NSData dataWithContentsOfFile:filePath]];
    return NSMakeSize(pdfImageRep.bounds.size.width, pdfImageRep.bounds.size.height);
}

+ (NSImage *)blankImageWithSize:(CGSize)size {
    NSImage* image = [[NSImage alloc] initWithSize:size];
    [image lockFocus];
    [[NSColor whiteColor] setFill];
    [NSBezierPath fillRect:NSMakeRect(0, 0, image.size.width, image.size.height)];
    [image unlockFocus];
    return image;
}

+ (NSImage *)blackImageWithSize:(CGSize)size
{
    NSImage* image = [[NSImage alloc] initWithSize:size];
    
    if (size.height == 0 || size.width == 0) {
        NSLog(@"skip it");
    }
    else {
//    NSImage* image = [[NSImage alloc] initWithSize:size];
    [image lockFocus];
    [[NSColor blackColor] setFill];
    [NSBezierPath fillRect:NSMakeRect(0, 0, image.size.width, image.size.height)];
    [image unlockFocus];
    return image;
    }
     return image;

}

+ (NSImage*) resizeImage:(NSImage*)sourceImage size:(NSSize)size
{
    NSRect targetFrame = NSMakeRect(0, 0, size.width, size.height);
    NSImage*  targetImage = [[NSImage alloc] initWithSize:size];
    
    [targetImage lockFocus];
    
    [sourceImage drawInRect:targetFrame
                   fromRect:NSZeroRect       //portion of source image to draw
                  operation:NSCompositeCopy  //compositing operation
                   fraction:1.0              //alpha (transparency) value
             respectFlipped:YES              //coordinate system
                      hints:@{NSImageHintInterpolation:
                                  [NSNumber numberWithInt:NSImageInterpolationLow]}];
    
    [targetImage unlockFocus];
    
    return targetImage;
}

+(NSImage *)makePDFimage:(NSInteger)numb withPath:(NSString *)filePath {
    @autoreleasepool {
        
        NSData* pdfData = [NSData dataWithContentsOfFile:filePath];
        NSPDFImageRep* pdfImageRep = [NSPDFImageRep imageRepWithData:pdfData];
        NSSize sizeX = NSMakeSize(pdfImageRep.bounds.size.width*2, pdfImageRep.bounds.size.height*2);
        [pdfImageRep setCurrentPage:numb];

        NSImage* scaledImage = [NSImage imageWithSize:sizeX flipped:NO drawingHandler:^BOOL(NSRect dstRect){
            [pdfImageRep drawInRect:dstRect];
            return YES;
        }];
        NSBitmapImageRep* pngImageRep = [NSBitmapImageRep imageRepWithData:[scaledImage TIFFRepresentation]];
        NSData* finalData = [pngImageRep representationUsingType:NSJPEGFileType properties:nil];
        NSImage *img = [[NSImage alloc] initWithData:finalData];
        
        return img;
    }
}



@end
