//
//  WindowController.m
//  Redactor
//
//  Created by William Palin on 4/8/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "WindowController.h"

@interface WindowController ()
- (IBAction)execute:(id)sender;
- (IBAction)markup:(id)sender;
- (IBAction)contentToggle:(id)sender;
- (IBAction)zoomIn:(id)sender;
@property (weak) IBOutlet NSSegmentedCell *segmentedCell;
@property (weak) IBOutlet NSToolbar *theToolbar;
@property (weak) IBOutlet NSToolbarItem *redact;

@end

@implementation WindowController

- (void)windowDidLoad {
    [super windowDidLoad];
    [self.window setFrame:NSMakeRect(10.f, 10.f, 800, 862) display:NO animate:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removeSearch)
                                                 name:@"removeSearch"
                                               object:nil];
  
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(windowClosing:)
//                                                 name:NSWindowWillCloseNotification
//                                               object:self];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(windowWillClose:) name:NSWindowWillCloseNotification object:self.window];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(windowWillClose:) name:NSWindowWillCloseNotification object:self.window];


}

- (void)removeSearch {
    [self.theToolbar removeItemAtIndex:5];
}

- (IBAction)toggleThumbnail:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"contentToggle" object:nil];
}

- (IBAction)execute:(id)sender {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"customRedact"
     object:nil];
}


- (IBAction)markup:(id)sender {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"toggleMarkup"
     object:nil];
}

- (IBAction)contentToggle:(id)sender {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"contentToggle"
     object:nil];
}

- (IBAction)zoomIn:(id)sender {
    
    NSLog(@"the number is %lu",self.segmentedCell.selectedSegment);
    if (self.segmentedCell.selectedSegment == 1) {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"zoomIn"
         object:nil];
    }
    else {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"zoomOut"
         object:nil];
    }
}

- (IBAction)toggleNames:(id)sender {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"names"
     object:nil];

}

- (IBAction)toggleDates:(id)sender {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"dates"
     object:nil];

}

- (IBAction)savePanel:(id)sender {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"save"
     object:nil];
}


- (IBAction)recreatePdf:(id)sender {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"recreate"
     object:nil];
}



- (IBAction)searchField:(id)sender {

}



//- (void)windowWillClose:(NSNotification *)notification {
///// how to stop it from closing
//    [self windowShouldClose:self.window];
//    
//}

//- (BOOL)windowShouldClose:(NSNotification *)notification {
//
//    return NO;
//}

//- (BOOL)windowShouldClose:(id)sender{
//    return NO;
//}



//-(void)mouseDragged:(NSEvent *)theEvent {
//    NSLog(@"mouse down");
////    [self showSimpleAlert];
//    
//}

//- (void)showSimpleAlert {
//    NSAlert *alert = [[NSAlert alloc] init];
//    [alert addButtonWithTitle:@"Save as Searchable PDF"];
//    [alert addButtonWithTitle:@"Save as Non-Searchable PDF"];
//    [alert addButtonWithTitle:@"Cancel"];
//    [alert setMessageText:@"Save Document as?"];
//    [alert setInformativeText:@"Press Okay."];
//
//    [alert setAlertStyle:NSWarningAlertStyle];
//    [alert beginSheetModalForWindow:[self window] modalDelegate:self didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];
//}

//- (void)alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
//    if (returnCode == NSAlertFirstButtonReturn) {
////        [self testQuartz];
//    }
//    else if (returnCode == NSAlertSecondButtonReturn) {
//        //        [self testQuartz];
//    }
//    else if (returnCode == NSAlertThirdButtonReturn) {
//        //        [self testQuartz];
//    }
//    
//}


@end
