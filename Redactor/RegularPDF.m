//
//  RegularPDF.m
//  Redactor
//
//  Created by William Palin on 4/17/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "RegularPDF.h"
#import "Word.h"

@implementation RegularPDF

+ (NSMutableArray *)createMutableArrayOfAllContentOnPage:(PDFPage *)pageAtIndex and:(NSMutableArray *)allVisibleContent {
    NSLinguisticTaggerOptions options =  NSLinguisticTaggerOmitWhitespace ;
    NSLinguisticTagger *tagger = [[NSLinguisticTagger alloc] initWithTagSchemes: [NSLinguisticTagger availableTagSchemesForLanguage:@"en"] options:options];
    tagger.string = pageAtIndex.string;
    
    [tagger enumerateTagsInRange:NSMakeRange(0, [pageAtIndex.string length]) scheme:NSLinguisticTagSchemeNameTypeOrLexicalClass options:options usingBlock:^(NSString *tag, NSRange tokenRange, NSRange sentenceRange, BOOL *stop) {
        NSRange range = NSMakeRange(0, 1);
        if([[[[[pageAtIndex selectionForRange:tokenRange] attributedString] attributesAtIndex:0 effectiveRange:&range] objectForKey:@"NSColor"] alphaComponent] == 1){
            [allVisibleContent addObject:[pageAtIndex selectionForRange:tokenRange]];
        }
    }];
    return allVisibleContent;
}

//this bool enumerates through the page content and counts how many objects are transparent.
+ (BOOL)doesPageHaveHiddenContent:(PDFPage *)pageAtIndex andArrayOfContent:(NSMutableArray *)opaqueContent and:(NSMutableArray *)allContent {
    
    NSLinguisticTaggerOptions options =  NSLinguisticTaggerOmitWhitespace ;
    NSLinguisticTagger *tagger = [[NSLinguisticTagger alloc] initWithTagSchemes: [NSLinguisticTagger availableTagSchemesForLanguage:@"en"] options:options];
    tagger.string = pageAtIndex.string;
    
    [tagger enumerateTagsInRange:NSMakeRange(0, [pageAtIndex.string length]) scheme:NSLinguisticTagSchemeNameTypeOrLexicalClass options:options usingBlock:^(NSString *tag, NSRange tokenRange, NSRange sentenceRange, BOOL *stop) {
        NSRange range = NSMakeRange(0, 1);
        
        if([[[[[pageAtIndex selectionForRange:tokenRange] attributedString] attributesAtIndex:0 effectiveRange:&range] objectForKey:@"NSColor"] alphaComponent] == 1){
            [opaqueContent addObject:[pageAtIndex selectionForRange:tokenRange]];
        }
        [allContent addObject:@"count"];
        
    }];
    if([allContent count]-[opaqueContent count] > 5){
        return YES;
    }
    return NO;
}





@end
