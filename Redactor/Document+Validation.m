//
//  Document+Validation.m
//  
//
//  Created by Brian on 6/13/15.
//
//

#import "Document+Validation.h"

@implementation Document (Validation)

- (BOOL)isCorrupt {
    for (Page *page in self.pages) {
        if (page.isPageCorrupted) {
            return YES;
        }
    }
    return NO;
}

@end
