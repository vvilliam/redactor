//
//  ViewController.m
//  Redactor
//
//  Created by William Palin on 4/8/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "ViewController.h"
#import <Quartz/Quartz.h>
#import <CoreGraphics/CoreGraphics.h>
#import "MakeImageFromPdfPage.h"
#import "TesseractObject.h"
#import "Page.h"
#import "Word.h"
#import "StartingViewController.h"
#import "MBProgressHUD.h"
#import "RegularPDF.h"
#import <CoreText/CoreText.h>

@import AppKit;

@interface ViewController ()

@property (weak) IBOutlet PDFThumbnailView *thumbnailView;
@property (weak) IBOutlet PDFView *pdfView;
@property (nonatomic, strong) Document *document;
@property (nonatomic, strong) NSMutableArray *dateAnnotations;
@property (nonatomic) BOOL ocred;

@end

@implementation ViewController

-(void)viewWillAppear {
    NSString *urlStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"thePath"];
    NSData *data = [[NSData alloc]initWithContentsOfFile:urlStr];
    PDFDocument *document = [[PDFDocument alloc]initWithData:data];
    
    self.pdfView.document = document;
    self.thumbnailView.PDFView = self.pdfView;
    [self.thumbnailView setThumbnailSize:NSMakeSize(85, 110)];
 
}
-(void)viewWillDisappear {
    
//    NSAlert *alert = [[NSAlert alloc] init];
//    [alert addButtonWithTitle:@"Done?"];
//    [alert addButtonWithTitle:@"Cancel"];
//    [alert setMessageText:@"Are you sure you're done?"];
//    [alert setInformativeText:@"Press Okay."];
//    [alert setAlertStyle:NSWarningAlertStyle];
//    if ([alert runModal] == NSAlertFirstButtonReturn) {
//        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"searchTerms"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"searchTerms"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerNotifications];
    [self.pdfView setAcceptsTouchEvents:YES];
    
    
   
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
}

-(void)viewDidAppear {
    self.ocred = NO;
//
    PDFPage *page0 = [self.pdfView.document pageAtIndex:0];

    NSCharacterSet *set = [NSCharacterSet alphanumericCharacterSet];
    NSRange alphanumeric = [page0.string rangeOfCharacterFromSet:set];
    

    if(page0.numberOfCharacters == 0){
        NSLog(@"blank page");
    }
    else if (alphanumeric.location == NSNotFound){
        NSLog(@"lets OCR this document for you");
        NSAlert *alert = [[NSAlert alloc] init];
        [alert addButtonWithTitle:@"OK"];
        [alert addButtonWithTitle:@"Cancel"];
        [alert setMessageText:@"This document must be OCR'd first?"];
        [alert setInformativeText:@"Press Okay to convert image to text."];
        [alert setAlertStyle:NSWarningAlertStyle];
        if ([alert runModal] == NSAlertFirstButtonReturn) {
            self.ocred = YES;
            [self removeSearch:@"self"];
            [self executeTesseract];
        }

    }
    
    else {
        NSLog(@"you are good to go");
        [self executeTesseractWithoutAnnotations];
//        [self executeTesseract];

    }
    

    
}

- (IBAction)removeSearch:(id)sender {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"removeSearch"
     object:nil];
}

- (void)registerNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(executeTesseract)
                                             name:@"execute"
                                             object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(zoomIn)
                                                 name:@"zoomIn"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(zoomOut)
                                                 name:@"zoomOut"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(highlightNames)
                                                 name:@"names"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(savePDF)
                                                 name:@"save"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideByLine)
                                                 name:@"customRedact"
                                               object:nil];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(findWords)
                                                 name:@"searchDocument"
                                               object:nil];
    
}

- (void)zoomIn{
    [self.pdfView zoomIn:@"self"];
    NSRect rect;
    [self toggleDataSelection:rect with:0];
}

- (void)zoomOut{
    [self.pdfView zoomOut:@"self"];
}

- (void)executeTesseract {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.pdfView animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Calculating";

    [self document:^(Document *document) {
        self.document = document;
        [self buildAnnotations:document];
        [self.pdfView goToFirstPage:@"self"];
        [MBProgressHUD hideHUDForView:self.pdfView animated:YES];
    }];
}

- (void)executeTesseractWithoutAnnotations {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.pdfView animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Analyzing";
    
    [self documentPDF:^(Document *document) {
        self.document = document;
        [self highlightStuff:document];

        [self.pdfView goToFirstPage:@"self"];
        [MBProgressHUD hideHUDForView:self.pdfView animated:YES];

    }];
}

+ (ImageConstructor)imageAtIndex:(NSInteger)index {
    ImageConstructor image = ^NSImage *{
        NSString *filePath = [[NSUserDefaults standardUserDefaults] objectForKey:@"thePath"];
        return [MakeImageFromPdfPage makePDFimage:index withPath:filePath];
    };
    return image;
}

- (void)document:(void (^)(Document *document))completionHandler {
    Document *document = [[Document alloc]init];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self collectDataWithUpdateHandler:^(Page *page, CGFloat progress) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [document addPage:page];
            });
        } andCompletionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(document);
            });
        }];
    });
}

-(void)collectDataWithUpdateHandler:(void (^)(Page *page, CGFloat progress))updateHandler andCompletionHandler:(void (^)(void))completionHandler {
    __block NSInteger progressCount = 0;
    NSInteger pageCount = self.pdfView.document.pageCount;
    
    for (NSInteger index = 0; index < pageCount; index++) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                updateHandler([TesseractObject pageFromImage:[ViewController imageAtIndex:index] withIndex:index], (CGFloat)(progressCount + 1)/(CGFloat)pageCount);
                progressCount++;
                if (progressCount == pageCount) {
                    completionHandler();
                }
            }
        });
    }
}
////alternative function for PDF documents
- (void)documentPDF:(void (^)(Document *document))completionHandler {
    Document *document = [[Document alloc]init];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self collectDataWithUpdateHandlerPDF:^(Page *page, CGFloat progress) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [document addPage:page];
            });
        } andCompletionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(document);
            });
        }];
    });
}

-(void)collectDataWithUpdateHandlerPDF:(void (^)(Page *page, CGFloat progress))updateHandler andCompletionHandler:(void (^)(void))completionHandler {
    __block NSInteger progressCount = 0;
    NSInteger pageCount = self.pdfView.document.pageCount;
    
    for (NSInteger index = 0; index < pageCount; index++) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                updateHandler([TesseractObject pageFromPDF:self.pdfView.document withIndex:index], (CGFloat)(progressCount + 1)/(CGFloat)pageCount);
                progressCount++;
                if (progressCount == pageCount) {
                    completionHandler();
                }
            }
        });
    }
}

-(void)highlightStuff:(Document *)document{
   
    for (NSInteger pageIndex = 0; pageIndex < [self.pdfView.document pageCount]; pageIndex++) {
        Page *page = [document pageAtIndex:pageIndex];
        PDFPage *pageR = [self.pdfView.document pageAtIndex:pageIndex];
       
        for (NSInteger wordIndex; wordIndex < [page.words count]; wordIndex++) {
            if([page.words[wordIndex] isTest] == 1) {
                [self drawAnnotation:page.words[wordIndex] onPage:pageR];
            }
        }
    }
}

- (void)drawAnnotation:(Word *)word onPage:(PDFPage *)pageX{
    NSRange range = word.rangeForWords;
    PDFSelection *selection = [pageX selectionForRange:range];
    NSRect rect = [selection boundsForPage:[self.pdfView currentPage]];
   
    NSPoint newPoint = NSMakePoint(rect.origin.x+(rect.size.width*.5), rect.origin.y+(rect.size.height*.5));
    PDFSelection *selectionAtAnnotation = [self getLength:[pageX selectionForWordAtPoint:newPoint]];
    NSRect rectX = [selectionAtAnnotation boundsForPage:[self.pdfView currentPage]];
    
    [[self.pdfView currentPage] addAnnotation:[self nameAnnotation:rectX]];
    [self.pdfView setNeedsDisplay: YES];

}



-(PDFAnnotationButtonWidget *)nameAnnotation:(NSRect)rect{
    PDFAnnotationButtonWidget *annotation = [[PDFAnnotationButtonWidget alloc] initWithBounds:rect];
    annotation.backgroundColor = [NSColor clearColor];
    annotation.color = [NSColor clearColor];
    annotation.fieldName = @"names";
    annotation.controlType = kPDFWidgetPushButtonControl;
    return annotation;
}


///////end of alternate history

- (void)buildAnnotations:(Document *)document{
    for (NSInteger pageCount = 0; pageCount < [self.pdfView.document pageCount]; pageCount++) {
        Page *page = [document pageAtIndex:pageCount];
    
        NSInteger pageIndex = pageCount;
        PDFPage *pageR = [self.pdfView.document pageAtIndex:pageIndex];
    
        if(self.ocred == YES){
            for (NSInteger index = 0; index < [page.words count]; index++) {
                Word *word = page.words[index];
                if(word.isSelected){
                    [self newAnnotation:pageR withWordObject:word andIsItSelected:1];
                }
                else{
                    [self newAnnotation:pageR withWordObject:word andIsItSelected:0];
                }
            }
        } else if(self.ocred == NO){
            for (NSInteger index = 0; index < [page.words count]; index++) {
                Word *word = page.words[index];
                if(word.isSelected){
                    [self newAnnotation:pageR withWordObject:word andIsItSelected:1];
                }
            }
        }
    }
}

- (void)newAnnotation: (PDFPage *)pageNumber withWordObject:(Word *)word andIsItSelected:(NSInteger)yn {
    NSRect		annotationBounds;
    NSRect		pageBounds;
    
    [self.pdfView goToPage:pageNumber];
    
    pageBounds = [[self.pdfView currentPage] boundsForBox: [self.pdfView displayBox]];
    annotationBounds = word.coordinate;
    if(pageBounds.size.height < pageBounds.size.width){
        annotationBounds = NSMakeRect((annotationBounds.origin.y),(612- annotationBounds.origin.x)-(annotationBounds.size.width), annotationBounds.size.height, annotationBounds.size.width);
    }
    
    PDFAnnotationButtonWidget *annotation;
    PDFPage *page = [self.pdfView currentPage];
    NSPoint newPoint = NSMakePoint(annotationBounds.origin.x+(annotationBounds.size.width*.5), annotationBounds.origin.y+(annotationBounds.size.height*.5));
    
    PDFSelection *selectionAtAnnotation = [self getLength:[page selectionForWordAtPoint:newPoint]];
    NSRect rectX = [selectionAtAnnotation boundsForPage:[self.pdfView currentPage]];
    
    if(self.ocred == YES){
        annotation = [[PDFAnnotationButtonWidget alloc] initWithBounds: annotationBounds];
    }
    else {
        annotation = [[PDFAnnotationButtonWidget alloc] initWithBounds: rectX];
    }
    
    if (yn==1) {
        annotation.backgroundColor = [NSColor clearColor];
        annotation.color = [NSColor clearColor];
        annotation.fieldName = @"names";
    }
    
    else {
        annotation.backgroundColor = [NSColor clearColor];
        annotation.color = [NSColor clearColor];
        annotation.fieldName = @"blue";

    }
    
    annotation.controlType = kPDFWidgetPushButtonControl;
    
    [[self.pdfView currentPage] addAnnotation:annotation];
    [self.pdfView setNeedsDisplay: YES];
}

-(PDFSelection *)getLength:(PDFSelection *)sel {
    PDFPage *activePage = [self.pdfView currentPage];
    CGFloat one = [sel boundsForPage:activePage].size.height;
    [sel extendSelectionAtEnd:1];
    
    NSString *commaRight = sel.string;
    if ([commaRight hasSuffix:@","] || [commaRight hasSuffix:@"."] || [commaRight hasSuffix:@"'"]) {
        [sel extendSelectionAtEnd:1];
    }
    
    CGFloat two = [sel boundsForPage:activePage].size.height;
    if (two > one) {
        [sel extendSelectionAtEnd:-1];
    }
    return sel;
    
}


- (void)newAnnotes:(PDFSelection *)selection andCurrentPage:(PDFPage *)currentPage {
    NSRect		annotationBounds;
    NSRect		pageBounds;
    
    
    [self.pdfView goToPage:currentPage];
    
    pageBounds = [[self.pdfView currentPage] boundsForBox: [self.pdfView displayBox]];
    NSRect rectX = [selection boundsForPage:[self.pdfView currentPage]];
    
    annotationBounds = rectX;
    
    if(pageBounds.size.height < pageBounds.size.width){
        NSRect rect = NSMakeRect((annotationBounds.origin.y),(612- annotationBounds.origin.x)-(annotationBounds.size.width), annotationBounds.size.height, annotationBounds.size.width);
        
        annotationBounds = rect;
    }
    
    PDFAnnotationButtonWidget *annotation;
    annotation = [[PDFAnnotationButtonWidget alloc] initWithBounds:annotationBounds];
    annotation.backgroundColor = [NSColor clearColor];
    annotation.color = [NSColor clearColor];
    annotation.fieldName = @"names";
    annotation.controlType = kPDFWidgetPushButtonControl;
    
    [[self.pdfView currentPage] addAnnotation:annotation];
    [self toggleSelection:annotation];
    [self.pdfView setNeedsDisplay: YES];
}


-(void)createNewButtonOnHighightedText {
    
    PDFSelection *cSelect = [self.pdfView currentSelection];
    NSRect rectX = [cSelect boundsForPage:[self.pdfView currentPage]];
    
    PDFAnnotationButtonWidget *annotation;
    annotation = [[PDFAnnotationButtonWidget alloc] initWithBounds:rectX];
    annotation.backgroundColor = [NSColor clearColor];
    annotation.color = [NSColor clearColor];
    annotation.fieldName = @"blue";
    annotation.controlType = kPDFWidgetPushButtonControl;
    
    [[self.pdfView currentPage] addAnnotation:annotation];
    [self toggleSelection:annotation];
    
    [self.pdfView setNeedsDisplay: YES];

}


-(void)hideByLine {
    
    PDFSelection *cSelect = [self.pdfView currentSelection];
    NSArray *selectionByLine = [cSelect selectionsByLine];

    for (NSInteger index = 0; index < [selectionByLine count]; index++) {
        NSRect oneRect = [selectionByLine[index] boundsForPage:[self.pdfView currentPage]];

        PDFAnnotationButtonWidget *annotation;
        annotation = [[PDFAnnotationButtonWidget alloc] initWithBounds:oneRect];
        annotation.backgroundColor = [NSColor clearColor];
        annotation.color = [NSColor clearColor];
        annotation.fieldName = @"blue";
        
        annotation.controlType = kPDFWidgetPushButtonControl;
        
        [[self.pdfView currentPage] addAnnotation:annotation];
        [self toggleSelection:annotation];
        [self.pdfView setNeedsDisplay: YES];
        
    }
}


- (void)findWords {
    NSString *searchTerms = [[NSUserDefaults standardUserDefaults] objectForKey:@"searchTerms"];
    PDFDocument *doc = self.pdfView.document;
    [self.pdfView setHighlightedSelections:[doc findString:searchTerms withOptions:NSCaseInsensitiveSearch]];
    
    Word *word = [[Word alloc]init];
    word.text = searchTerms;
    word.rangeForWords = NSMakeRange(572, 3);
    PDFPage *pageR = [self.pdfView.document pageAtIndex:0];
    [self drawAnnotation:word onPage:pageR];
    
//    - (PDFAction *) mouseUpAction;
//    - (void) setMouseUpAction: (PDFAction *) action;

    
}





- (void)mouseUp:(NSEvent *)theEvent{
    

    PDFAnnotationButtonWidget *newActiveAnnotation = NULL;
    
    _mouseDownLoc = [self.pdfView convertPoint: [theEvent locationInWindow] fromView: NULL];
    PDFPage *activePage = [self.pdfView pageForPoint: _mouseDownLoc nearest:YES];
    NSPoint pagePoint = [self.pdfView convertPoint: _mouseDownLoc toPage:activePage];
    NSRect	annotationBounds;
    
    

    NSArray *annotations = [activePage annotations];
    NSInteger numAnnotations = [annotations count];
    for (NSInteger index = 0; index < numAnnotations; index++){
        
        annotationBounds = [[annotations objectAtIndex: index] bounds];
        if (NSPointInRect(pagePoint, annotationBounds)) {
            newActiveAnnotation = [annotations objectAtIndex: index];
            _clickDelta.x = pagePoint.x - annotationBounds.origin.x;
            _clickDelta.y = pagePoint.y - annotationBounds.origin.y;
        }
    }
    
    if(self.ocred == NO){
        if (!(newActiveAnnotation == NULL)) {
            [activePage removeAnnotation:newActiveAnnotation];
            [self.pdfView setNeedsDisplay: YES];
        }
    }
    else{
        [self toggleSelection:newActiveAnnotation];
        [self selectAnnotation:newActiveAnnotation];
    }
    if (newActiveAnnotation == NULL) {
        [super mouseDown: theEvent];
        
        PDFSelection *sel = [activePage selectionForWordAtPoint:pagePoint];
        CGFloat one = [sel boundsForPage:activePage].size.height;
        [sel extendSelectionAtEnd:1];
        
        NSString *commaRight = sel.string;
        if ([commaRight hasSuffix:@","] || [commaRight hasSuffix:@"."] || [commaRight hasSuffix:@"'"]) {
            [sel extendSelectionAtEnd:1];
        }
        
        CGFloat two = [sel boundsForPage:activePage].size.height;
        if (two > one) {
            [sel extendSelectionAtEnd:-1];
        }

        
        NSCharacterSet *s = [NSCharacterSet characterSetWithCharactersInString:@"\"abcdefghijk§lmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_.,*&@#$%'?!-)([]:/|"];
        
        NSString *stri = sel.string;
        NSRange r = [stri rangeOfCharacterFromSet:s];
        
        NSCharacterSet *set = [NSCharacterSet alphanumericCharacterSet];
        NSRange alphanumeric = [stri rangeOfCharacterFromSet:set];
        
        NSLog(@"the alpha numeric %lu",alphanumeric.location);
        
        if(!(r.location > 1)){
            [self newAnnotes:sel andCurrentPage:activePage];
        }
    }

}

- (void)toggleDataSelection:(NSRect)annoBounds with:(NSInteger)index {
    Page *page = [self.document pageAtIndex:index];
    NSArray *array = page.words;
    for (Word *word in array) {
        if (word.coordinate.origin.x == annoBounds.origin.x && word.coordinate.origin.y == annoBounds.origin.y) {
            [word toggleSelection];
        }
    }
}

- (void)selectAnnotation:(PDFAnnotation *) annotation {
    if (_activeAnnotation) {
        _wasBounds = [_activeAnnotation bounds];
    }
}


- (void)toggleSelection:(PDFAnnotationButtonWidget *)anno {
    CGFloat color = anno.backgroundColor.alphaComponent;
    if(color > 0){
        [anno setBackgroundColor:[NSColor clearColor]];
        [self.pdfView setNeedsDisplay: YES];
    }
    else {
        [anno setBackgroundColor:[NSColor colorWithCalibratedWhite:0.000 alpha:0.440]];
        NSLog(@"the anno is %f",anno.bounds.origin.x);
        NSLog(@"the anno is %f",anno.bounds.origin.x);

        [self.pdfView setNeedsDisplay: YES];
    }
}


- (void)hideWords:(PDFAnnotationButtonWidget *)anno {
    CGFloat color = anno.backgroundColor.alphaComponent;
    if(color > 0){
        [anno setBackgroundColor:[NSColor whiteColor]];
        [anno setColor:[NSColor whiteColor]];
        [self.pdfView setNeedsDisplay: YES];
    }
    else {
        [anno setBackgroundColor:[NSColor whiteColor]];
        [anno setColor:[NSColor whiteColor]];
        [self.pdfView setNeedsDisplay: YES];
    }
}

- (void)removeHiddenAnnotations {
    for (NSInteger pageCount = 0; pageCount < [self.pdfView.document pageCount]; pageCount++) {
        for (PDFAnnotationButtonWidget *anno in [[[self.pdfView.document pageAtIndex:pageCount] annotations] reverseObjectEnumerator]) {
            if(anno.backgroundColor.alphaComponent == 0) {
                [anno setBackgroundColor:[NSColor clearColor]];
                [[self.pdfView.document pageAtIndex:pageCount] removeAnnotation:anno];
            }
        }
    }
}

-(void)highlightNames {
    
    self.isSelected = !self.isSelected;  // toggle method
    if(self.isSelected){
        for (NSInteger pageCount = 0; pageCount < [self.pdfView.document pageCount]; pageCount++) {
            NSArray *annotations = [[self.pdfView.document pageAtIndex:pageCount] annotations];
            NSInteger count = [annotations count];
            
            for (NSInteger index = 0; index < count; index++){
                PDFAnnotationButtonWidget *anno = annotations[index];
                NSString *string = anno.fieldName;
                if([string isEqual:@"names"]) {
                    [anno setBackgroundColor:[NSColor colorWithCalibratedWhite:0.000 alpha:0.440]];
                    Page *page = [self.document pageAtIndex:pageCount];
                    Word *word = page.words[index];
                    word.isRedacted = YES;
                }
            }
            [self.pdfView setNeedsDisplay:YES];
        }
        [self.pdfView setNeedsDisplay:YES];
        
    }
    else {
        for (NSInteger pageCount = 0; pageCount < [self.pdfView.document pageCount]; pageCount++) {
            NSArray *annotations = [[self.pdfView.document pageAtIndex:pageCount] annotations];
            NSInteger count = [annotations count];
            
            for (NSInteger index = 0; index < count; index++){
                PDFAnnotationButtonWidget *anno = annotations[index];
                NSString *string = anno.fieldName;
                if([string isEqual:@"names"]) {
                    [anno setBackgroundColor:[NSColor clearColor]];
                }
            }
        }

    }
    [self.pdfView setNeedsDisplay:YES];
}


- (void)savePDF {
    if(self.ocred == YES){[self saveMe];}
    else {[self testQuartz];}
}

-(void)saveMe {
    for (NSInteger index = 0; index < [self.pdfView.document pageCount]; index++) {
        NSArray *annotations = [[self.pdfView.document pageAtIndex:index] annotations];
        for (PDFAnnotationButtonWidget *anno in annotations) {
            CGFloat color = anno.backgroundColor.alphaComponent;
            if (color > 0) {
                [anno setBackgroundColor:[NSColor blackColor]];
            }
        }
        [self.pdfView setNeedsDisplay:YES];
    }
    
    PDFDocument *pdf = [[PDFDocument alloc] init];
    for (NSInteger index = 0; index < [self.pdfView.document pageCount]; index++) {
        
        PDFPage *page = [self.pdfView.document pageAtIndex:index];
        NSImage *temp = [[NSImage alloc] initWithSize:[MakeImageFromPdfPage giveMeSize:[[NSUserDefaults standardUserDefaults] objectForKey:@"thePath"]]];
        [temp lockFocus];
        [page drawWithBox:kPDFDisplayBoxMediaBox];
        [temp unlockFocus];
        
        NSBitmapImageRep *rep = [NSBitmapImageRep imageRepWithData:[temp TIFFRepresentation]];
        NSData *finalData = [rep representationUsingType:NSJPEGFileType properties:nil];
        NSImage *img = [[NSImage alloc] initWithData:finalData];
        PDFPage *pageX = [ViewController pageFromImage:img];
        [pdf insertPage:pageX atIndex:index];
    }
    
    NSSavePanel *savePanel = [[NSSavePanel alloc] init];
    [savePanel setAllowedFileTypes:[NSArray arrayWithObject:@"pdf"]];
    
    if ([savePanel runModal] == YES) {
        [pdf writeToFile:[savePanel filename]];
    }

}

+ (PDFPage *)pageFromImage:(NSImage *)image {
    return [[PDFPage alloc]initWithImage:image];
}





- (void)testQuartz{
    [self removeHiddenAnnotations];
    
    NSString *urlStr = @"/Users/Palin/Desktop/blank.pdf";
    NSData *finalData = [[NSData alloc]initWithContentsOfFile:urlStr];

    CGDataProviderRef dataProvider = CGDataProviderCreateWithCFData((CFDataRef)finalData);
    CGPDFDocumentRef document = CGPDFDocumentCreateWithProvider(dataProvider);
    CFMutableDataRef mutableData = CFDataCreateMutable(NULL, 0);
    CGDataConsumerRef dataConsumer = CGDataConsumerCreateWithCFData(mutableData);
    CGPDFPageRef page = CGPDFDocumentGetPage(document, 1);
    CGRect pageRect = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
    CGContextRef pdfContext = CGPDFContextCreate(dataConsumer, &pageRect, NULL);
    
    NSData *dI;
    
    for (NSInteger index = 0; index < [self.pdfView.document pageCount]; index++) {
        
        NSData *data = [self getDataImage:index withImage:[NSImage imageNamed:@"blank.png"]];
        NSImage *img = [[NSImage alloc]initWithData:data];
       
        CGImageSourceRef source = CGImageSourceCreateWithData((CFDataRef)data, NULL);
        CGContextBeginPage(pdfContext,nil);
        CGContextDrawImage(pdfContext, CGRectMake(0, 0, [img size].width, [img size].height), CGImageSourceCreateImageAtIndex(source, 0, NULL));

        Page *page = [self.document pageAtIndex:index];
        
        for (NSInteger indx = 0; indx < [page.blackBars count]; indx++) {
            NSRect rect = [page.blackBars[indx]boundsForPage:[self.pdfView.document pageAtIndex:index]];
            
            if([[page.blackBars[indx] string] rangeOfCharacterFromSet:[self characterSets]].location != NSNotFound && !([[page.blackBars[indx] string]  isEqual: @"+"]) && !([[self.pdfView.document pageAtIndex:index] annotationAtPoint:CGPointMake(rect.origin.x, rect.origin.y)])){
                
                CTLineRef displayLine = CTLineCreateWithAttributedString((__bridge CFAttributedStringRef)[page.blackBars[indx] attributedString]);
                CGContextSetTextPosition(pdfContext, rect.origin.x, rect.origin.y+2.5);
                CTLineDraw(displayLine, pdfContext );
                CFRelease(displayLine );
            }
        }
        
        CGContextEndPage(pdfContext);
    }
    CGContextRelease(pdfContext);
    dI = (__bridge NSData *)(mutableData);
    CFRelease(mutableData);
    
    NSSavePanel *savePanel = [[NSSavePanel alloc] init];
    [savePanel setAllowedFileTypes:[NSArray arrayWithObject:@"pdf"]];
    
    if ([savePanel runModal] == YES) {
        [dI writeToFile:[savePanel filename] atomically:YES];
    }
}




- (NSCharacterSet *)characterSets {
    NSCharacterSet *charactersToCheck = [NSCharacterSet characterSetWithCharactersInString:@"\"abcdefghijk§lmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123+4567890_.,*&@#$%'?!-)([]:/|"];
    return charactersToCheck;
    
}


- (NSData *)getDataImage:(NSInteger)indx withImage:(NSImage *)image {
    NSData *dI;
    Page *page = [self.document pageAtIndex:indx];
    [self.document pageAtIndex:indx].blackBars = [RegularPDF bringByArray:self.pdfView.document atIndex:indx];

    NSString *blankPdfFilePath = [[NSBundle mainBundle] pathForResource:@"bb" ofType:@"pdf"];
    NSImage *anImage = [MakeImageFromPdfPage makePDFimage:0 withPath:blankPdfFilePath];
    
    for (NSInteger index = 0; index < [page.blackBars count]; index++) {
        NSRect rect = [page.blackBars[index]boundsForPage:[self.pdfView.document pageAtIndex:indx]];
        
        if ([[self.pdfView.document pageAtIndex:indx] annotationAtPoint:CGPointMake(rect.origin.x, rect.origin.y)]) {
            NSRect boundsAnno = [[self.pdfView.document pageAtIndex:indx] annotationAtPoint:CGPointMake(rect.origin.x, rect.origin.y)].bounds;
            [anImage lockFocus];
            [[NSImage imageNamed:@"blackBar.png"] drawInRect:boundsAnno];
            [anImage unlockFocus];
            dI = anImage.TIFFRepresentation;
        }
    }
    return dI;
}


-(NSPoint)averageRect:(PDFSelection *)selection {
    NSRect rectX = [selection boundsForPage:[self.pdfView currentPage]];
    NSPoint point = rectX.origin;
    CGFloat oldX = point.x;
    CGFloat oldY = point.y;
    CGFloat height = rectX.size.height;
    CGFloat width = rectX.size.width;
    
    CGFloat newX = oldX + width*0.5;
    CGFloat newY = oldY + height*0.5;
    
    NSPoint newPoint = NSMakePoint(newX, newY);
    return newPoint;
}


-(void)mouseDown:(NSEvent *)theEvent{
    NSLog(@"yes");
}



@end
    
    
