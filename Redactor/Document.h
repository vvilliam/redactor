//
//  PageResult.h
//  RedactPro
//
//  Created by William Palin on 3/13/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

@import Foundation;
@import Quartz;

#import "Page.h"

@interface Document : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic) BOOL ocred;
@property (nonatomic) BOOL orderedTesseract;
@property (nonatomic) BOOL documentCorruption;
@property (nonatomic) BOOL mustSaveNonSearchable;


@property (nonatomic, copy) NSMutableArray *allRecognizedNames;
@property (nonatomic) NSInteger pageCount;
@property (nonatomic, copy) PDFDocument *(^PDFDocument)(void);
@property (nonatomic) NSInteger numberOfWordsInDocument;


- (NSArray *)pages;
- (void)addPage:(Page *)page;
- (Page *)pageAtIndex:(NSInteger)index;


@end


//views are expensive - method that calls and creates the method it will run faster-
///so i need to create state of buttons that are related to the specific to the page.  - checked off buttons...
//
//nsbutton that uses blocks to - add the list of words - ... to call back to the page - in our data? model.