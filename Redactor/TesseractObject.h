//
//  TesseractObject.h
//  TestWords
//
//  Created by William Palin on 3/13/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Quartz/Quartz.h>
#import "LinguisticTagger.h"
#import "Page.h"
#import "Document.h"
#import "Tesseract.h"
#import <QuartzCore/QuartzCore.h>

@interface TesseractObject : NSObject <TesseractDelegate>

+ (Page *)pageFromImage:(NSImage *(^)(void))image withIndex:(NSInteger)integer;
+ (NSMutableArray *)text:(NSArray *)tessData;

+(Page *)pageFromPDF:(PDFDocument *)document withIndex:(NSInteger)index;


@property (nonatomic, strong) Document *document;


@end
