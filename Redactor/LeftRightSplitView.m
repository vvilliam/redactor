//
//  LeftRightSplitView.m
//  Redactor
//
//  Created by William Palin on 4/8/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "LeftRightSplitView.h"
#import "NSSplitView+DMAdditions.h"

@implementation LeftRightSplitView

- (instancetype)initWithCoder:(NSCoder *)coder{
    self = [super initWithCoder:coder];
    if (self){
        self.delegate = self;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(toggle)
                                                     name:@"contentToggle"
                                                   object:nil];
        [self setPosition:0 ofDividerAtIndex:0];
    }
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (CGFloat)splitView:(NSSplitView *)splitView constrainMaxCoordinate:(CGFloat)proposedMaximumPosition ofSubviewAt:(NSInteger)dividerIndex{
    return 120;
}

- (void)toggle{
    self.isSelected = !self.isSelected;
    [self setPosition: self.isSelected ? 120.0 : 0.0 ofDividerAtIndex:0 animated:YES];
}

@end
