//
//  Document+Validation.h
//  
//
//  Created by Brian on 6/13/15.
//
//

#import "Document.h"

@interface Document (Validation)

- (BOOL)isCorrupt;

@end
