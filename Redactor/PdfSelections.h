//
//  PdfSelections.h
//  Redactor
//
//  Created by William Palin on 5/18/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <Quartz/Quartz.h>
#import "Word.h"

@interface PdfSelections : NSObject

+ (PDFSelection *)newSelection:(PDFPage *)activePage atThePoint:(NSPoint)pagePoint;
+ (NSColor *)theOpaqueColor;
+ (NSPoint)averagePoint:(NSRect)rect;
+ (void)removeAnnotation:(Word *)word onPage:(PDFPage *)pageX;
+ (void)drawAnnotation:(Word *)word onPage:(PDFPage *)pageX;
+ (PDFSelection *)getLength:(PDFSelection *)sel atPage:(PDFPage *)pageX;
+ (PDFAnnotationButtonWidget *)nameAnnotation:(NSRect)rect;
+ (void)removeHiddenAnnotations:(PDFDocument *)document;
+ (void)cleanPdfDocument:(PDFDocument *)document;

@end
