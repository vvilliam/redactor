//
//  Word.h
//  RedactPro
//
//  Created by William Palin on 3/18/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Word : NSObject

@property (nonatomic, copy) NSString *text;
@property (nonatomic, readonly) NSString *textWithoutSymbols;
@property (nonatomic, readonly) NSString *noPunctuation;


@property (nonatomic, assign) NSArray *confidence;
@property (nonatomic, assign) NSRect coordinate;
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL isSelectedAddress;

@property (nonatomic, assign) NSRange rangeForWords;


-(void)toggleSelection;

@property (nonatomic, copy) NSMutableArray *togglePerWord; //array of document text broken down by line
@property (nonatomic, copy) void (^selectionHandler) (BOOL isSelected);

@property (nonatomic, assign) NSInteger index;


@property (nonatomic, assign) BOOL isRedacted;
@property (nonatomic, assign) BOOL isTest;

@property (nonatomic, assign) NSString *partOfSpeech;
@property (nonatomic, assign) BOOL capitalized;
@property (nonatomic, assign) BOOL noun;

@property (nonatomic, assign) BOOL isPhoneNumber;
@property (nonatomic, assign) BOOL isDate;
@property (nonatomic, assign) BOOL isAddress;
@property (nonatomic, assign) BOOL isLink;

@property (nonatomic, assign) BOOL isSSN;

@property (nonatomic, copy) NSString *typeOfWord;

@end


