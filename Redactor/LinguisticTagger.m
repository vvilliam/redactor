//
//  LinguisticTagger.m
//  RedactPro
//
//  Created by William Palin on 3/14/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "LinguisticTagger.h"

@implementation LinguisticTagger

+ (NSMutableArray *)fullNames:(NSString *)string {
    NSMutableArray *array = [[NSMutableArray alloc]init];

    NSLinguisticTaggerOptions options = NSLinguisticTaggerOmitWhitespace | NSLinguisticTaggerOmitPunctuation | NSLinguisticTaggerJoinNames;
    NSLinguisticTagger *tagger = [[NSLinguisticTagger alloc] initWithTagSchemes: [NSLinguisticTagger availableTagSchemesForLanguage:@"en"] options:options];
    tagger.string = string;
    [tagger enumerateTagsInRange:NSMakeRange(0, [string length]) scheme:NSLinguisticTagSchemeNameTypeOrLexicalClass options:options usingBlock:^(NSString *tag, NSRange tokenRange, NSRange sentenceRange, BOOL *stop) {
        NSString *token = [string substringWithRange:tokenRange];
        NSLog(@"%@: %@", token, tag);
        
        
    }];
    return array;
}

+ (NSMutableArray *)phoneNumbers:(NSString *)string {
  
    NSError *error = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber
                                                               error:&error];
   
    NSMutableArray *mutableArray = [[NSMutableArray alloc]init];

    [detector enumerateMatchesInString:string
                               options:kNilOptions
                                 range:NSMakeRange(0, [string length])
                            usingBlock:
     ^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {

         NSString *stringResult = [string substringWithRange:result.range];
         
         if ([[stringResult substringWithRange:NSMakeRange(3, 1)] isEqualToString: @"-" ] && [[stringResult substringWithRange:NSMakeRange(6, 1)] isEqualToString: @"-" ]) {
         }
         else {
         NSString *str = [string substringWithRange:result.range];
         NSArray *arr = [str componentsSeparatedByString:@" "];
         [mutableArray addObjectsFromArray:arr];
     }
     }];
    
    return mutableArray;
}


+ (NSMutableArray *)SSNs:(NSString *)string {
    
    NSError *error = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber
                                                               error:&error];
    
    NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
    
    [detector enumerateMatchesInString:string
                               options:kNilOptions
                                 range:NSMakeRange(0, [string length])
                            usingBlock:
     ^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
         
         NSString *stringResult = [string substringWithRange:result.range];
         
         if ([[stringResult substringWithRange:NSMakeRange(3, 1)] isEqualToString: @"-" ] && [[stringResult substringWithRange:NSMakeRange(6, 1)] isEqualToString: @"-" ]) {
             
             NSString *str = [string substringWithRange:result.range];
//             NSArray *arr = [str componentsSeparatedByString:@" "];
             [mutableArray addObject:[str substringWithRange:NSMakeRange(0, 11)]];
//             NSLog(@"the ssn is %@",str);

         }
     }];
    NSLog(@"The array is %@",mutableArray);
    
    return mutableArray;
}




+ (NSMutableArray *)addresses:(NSString *)string {
    NSError *error = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeAddress
                                                               error:&error];
    
    NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
    
    [detector enumerateMatchesInString:string
                               options:kNilOptions
                                 range:NSMakeRange(0, [string length])
                            usingBlock:
     ^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
         
         NSString *str = [string substringWithRange:result.range];
         NSLog(@"Match: %@ and %llu", str, result.resultType);
         NSArray *arr = [str componentsSeparatedByString:@" "];
         
         [mutableArray addObjectsFromArray:arr];
     }];
    
    return mutableArray;
}

+ (NSMutableArray *)dates:(NSString *)string {
    NSError *error = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeDate
                                                               error:&error];
    
    NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
    
    [detector enumerateMatchesInString:string
                               options:kNilOptions
                                 range:NSMakeRange(0, [string length])
                            usingBlock:
     ^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
         
         NSString *str = [string substringWithRange:result.range];
         NSLog(@"Match: %@ and %llu", str, result.resultType);
         NSArray *arr = [str componentsSeparatedByString:@" "];
         
         [mutableArray addObjectsFromArray:arr];
     }];
    
    return mutableArray;
}


+ (NSMutableArray *)lastNames:(NSString *)strings {
    NSLinguisticTaggerOptions options = NSLinguisticTaggerOmitWhitespace | NSLinguisticTaggerOmitPunctuation ;
    NSLinguisticTagger *tagger = [[NSLinguisticTagger alloc] initWithTagSchemes: [NSLinguisticTagger availableTagSchemesForLanguage:@"en"] options:options];
    tagger.string = strings;
    NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
    
    [tagger enumerateTagsInRange:NSMakeRange(0, [strings length]) scheme:NSLinguisticTagSchemeLexicalClass options:options usingBlock:^(NSString *tag, NSRange tokenRange, NSRange sentenceRange, BOOL *stop) {
        NSString *token = [strings substringWithRange:tokenRange];
        BOOL isUppercase = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:[token characterAtIndex:0]];
        
//        NSLog(@"the token range is %lu and %lu",(unsigned long)tokenRange.location, (unsigned long)tokenRange.length);
//        NSLog(@"the toen is %@",token);
        
        if(([tag isEqualTo:@"Noun"] || [tag isEqualTo:@"OtherWord"]) && isUppercase){
            [mutableArray addObject:token];
        }
    }];
    
    NSString *cWords = [[NSBundle mainBundle] pathForResource:@"commonWords" ofType:@"csv"];
    NSString *wordData = [NSString stringWithContentsOfFile:cWords encoding:NSUTF8StringEncoding error:nil];
    NSMutableCharacterSet *cwordsKeep = [NSMutableCharacterSet alphanumericCharacterSet];
    [cwordsKeep addCharactersInString:@","];
    NSCharacterSet *cRemove = [cwordsKeep invertedSet];
    NSString *tReplace = [[wordData componentsSeparatedByCharactersInSet:cRemove] componentsJoinedByString:@""];
    NSArray* cRows = [tReplace componentsSeparatedByString:@","];
    
    
//    NSString *rdDP = [[NSBundle mainBundle] pathForResource:@"LastNames" ofType:@"csv"];
//    NSString *dataStr = [NSString stringWithContentsOfFile:rdDP encoding:NSUTF8StringEncoding error:nil];
//    
    NSMutableCharacterSet *charactersToKeep = [NSMutableCharacterSet alphanumericCharacterSet];
    [charactersToKeep addCharactersInString:@","];
    NSCharacterSet *charactersToRemove = [charactersToKeep invertedSet];
//    NSString *trimmedReplacement = [[dataStr componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
//    NSArray* rows = [trimmedReplacement componentsSeparatedByString:@","];
    
    
    NSString *fNames = [[NSBundle mainBundle] pathForResource:@"listOfNamesNoSpace" ofType:@"csv"];
    NSString *fString = [NSString stringWithContentsOfFile:fNames encoding:NSUTF8StringEncoding error:nil];
    NSMutableCharacterSet *fWordsToKeep = [NSMutableCharacterSet alphanumericCharacterSet];
    [fWordsToKeep addCharactersInString:@","];
    NSString *tRstring = [[fString componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    NSArray* oRows = [tRstring componentsSeparatedByString:@","];
    
    NSMutableArray *surnamesArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < [mutableArray count]; i++) {
        BOOL contains =  [[LinguisticTagger commonLastNamesStatic] containsObject:[mutableArray[i] uppercaseString]];
        
        BOOL contains2 = [cRows containsObject:[mutableArray[i] lowercaseString]];
        BOOL contains3 = [oRows containsObject:[mutableArray[i] uppercaseString]];

        if((contains & !contains2) || (contains3 & !contains2)){
            [surnamesArray addObject: mutableArray[i]];
        }
    }
    return surnamesArray;
}

+ (NSArray *)commonLastNamesStatic {
    static NSArray *array;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        array = [LinguisticTagger commonLastNames];
    });
    return array;
}


+ (NSArray *)commonLastNames{
    NSString *rdDP = [[NSBundle mainBundle] pathForResource:@"LastNames" ofType:@"csv"];
    NSString *dataStr = [NSString stringWithContentsOfFile:rdDP encoding:NSUTF8StringEncoding error:nil];
    NSMutableCharacterSet *charactersToKeep = [NSMutableCharacterSet alphanumericCharacterSet];
    [charactersToKeep addCharactersInString:@","];
    NSCharacterSet *charactersToRemove = [charactersToKeep invertedSet];
    NSString *trimmedReplacement = [[dataStr componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    NSArray* rows = [trimmedReplacement componentsSeparatedByString:@","];
    return rows;
}

@end
