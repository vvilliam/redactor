//
//  AppDelegate.m
//  Redactor
//
//  Created by William Palin on 4/8/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

@import AppKit;
@import Quartz;

#import "AppDelegate.h"
#import "RDWindow.h"
#import "PdfSelections.h"
#import "Document+Construction.h"
#import "Document+Validation.h"
#import "NSOpenPanel+Construction.h"
#import "NSAlert+Construction.h"
#import "PDFDocument+Validation.h"
#import "PDFDocument+Rasterization.h"
#import "PDFDocument+Construction.h"
#import "PDFView+Validation.h"

@interface AppDelegate () <NSWindowDelegate>

@property (nonatomic, copy) NSSet *windows;
@property (nonatomic, weak) IBOutlet NSMenuItem *menuItemFullscreen;

@end

@implementation AppDelegate

#pragma mark - Open-dialogue handling from dock icon selection

- (BOOL)applicationShouldOpenUntitledFile:(NSApplication *)sender {
    return YES;
}

- (BOOL)applicationOpenUntitledFile:(NSApplication *)theApplication {
    if (self.windows.count <= 0) {
//        NSOpenPanel *openPanel = [NSOpenPanel PDFOpenPanel];
        //        if ([openPanel runModal] == YES) {
        //            [self openDocumentAtURL:openPanel.URL];
        //        }
    }
    return YES;
}

#pragma mark -

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    NSOpenPanel *openPanel = [NSOpenPanel PDFOpenPanel];
    if ([openPanel runModal] == YES) {
        [self openDocumentAtURL:openPanel.URL];
    }
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    [self closeAllWindowsWithCompletionHandler:^{
        [[NSApplication sharedApplication] setDelegate:nil];
        [[NSApplication sharedApplication] terminate:self];
    }];
    return NO;
}

- (BOOL)application:(NSApplication *)sender openFile:(NSString *)filename {
    [self openDocumentAtURL:[NSURL fileURLWithPath:filename]];
    return YES;
}

- (IBAction)newDocument:(id)sender {
    NSOpenPanel *openPanel = [NSOpenPanel PDFOpenPanel];
    if ([openPanel runModal] == YES) {
        [self openDocumentAtURL:openPanel.URL];
    }
}

- (void)openDocumentAtURL:(NSURL *)URL {
    PDFDocument *PDF = [PDFDocument documentFromFileWithURL:URL];
    if (PDF) {
        [[NSDocumentController sharedDocumentController] noteNewRecentDocumentURL:URL];
        RDWindow *window = self.newWindow;
        [self addWindow:window];
        [window makeKeyAndOrderFront:self];
        [window beginAnalyzingState:@"Analyzing"];
        
        PDFDocument *PDF = [PDFDocument documentFromFileWithURL:URL];
        
        if (PDF.isText) {
            [Document documentFromPDFDocumentConstructor:^{
                return PDF;
            } withCompletionHandler:^(Document *document) {
                document.title = URL.lastPathComponent;
                document.ocred = NO;
                [window setDocument:document];
                [window finishAnalyzingState];
                if (document.isCorrupt || window.PDFView.isPossiblySearchable) {
                    document.mustSaveNonSearchable = YES;
                }
            }];
        } else {
            [Document documentFromPDFDocumentConstructor:^{
                return PDF;
            } withCompletionHandler:^(Document *document) {
                document.title = URL.lastPathComponent;
                document.ocred = NO;
                [window setDocument:document];
                [window finishAnalyzingState];
                [self ocrDocument:@"self"];
            }];
        }
    } else {
        // Document is unavailable. Notify user.
    }
}


- (IBAction)redactNames:(id)sender {
    [self.activeWindow toggleNameHighlights];
}

- (IBAction)redactDates:(id)sender {
    [self.activeWindow toggleDateHighlights];
}

- (IBAction)redactAddresses:(id)sender {
    [self.activeWindow toggleAddressHighlights];
}

- (IBAction)redactPhoneNumbers:(id)sender {
    [self.activeWindow togglePhoneHighlights];
}

- (IBAction)redactSSNs:(id)sender {
    [self.activeWindow toggleSSNHighlights];
}

- (IBAction)redactHighlights:(id)sender {
    [self.activeWindow redactHighlights];
}

- (IBAction)redactLinks:(id)sender {
    [self.activeWindow toggleLinkHighlights];
}

- (IBAction)didReceiveRedactCurrentPageEventFromControl:(NSMenuItem *)item {
    [self.activeWindow redactCurrentPage];
}

- (IBAction)ocrDocument:(id)sender {
    RDWindow *window = self.activeWindow;
    [window beginAnalyzingState:@"Extracting Text from Image"];
    PDFDocument *document = window.document.PDFDocument();
    /*
     Constructor may be access from multiple threads.
     Pass only direct pointer to document, not pointer to pointer to object (ie. self.window.docuemnt).
     */
    [Document imageDocumentFromPDFDocumentConstructor:^{
        return document;
    } completionHandler:^(Document *document) {
        document.ocred = YES;
        document.orderedTesseract = YES;
        document.title = window.document.title;
        [window setDocument:document];
        [window finishAnalyzingState];
    }];
}

- (IBAction)savePDF:(id)sender {
    [self savePDFWithCompletionHandler:^{}];
}

- (void)savePDFWithCompletionHandler:(void (^)(void))completionHandler {
    RDWindow *window = self.activeWindow;
    if (window.document.ocred || window.document.mustSaveNonSearchable ) {
        [self saveWindow:window];
    } else {
        if ([[NSAlert alertForFormatSelection] runModal] == NSAlertFirstButtonReturn) {
            NSSavePanel *savePanel = [[NSSavePanel alloc] init];
            [savePanel setAllowedFileTypes:[NSArray arrayWithObject:@"pdf"]];
            if ([savePanel runModal] == YES) {
                [window beginExportingState];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [window.document.searchablePDF writeToURL:savePanel.URL atomically:YES];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [window finishExportingState];
                    });
                });
            }
        } else {
            [self saveWindow:window];
        }
    }
    if (completionHandler) {
        completionHandler();
    }
}

- (void)saveWindow:(RDWindow *)window {
    [window finalizeRedactions];
    NSSavePanel *savePanel = [[NSSavePanel alloc] init];
    [savePanel setAllowedFileTypes:[NSArray arrayWithObject:@"pdf"]];
    
    if ([savePanel runModal] == YES) {
        [window beginExportingState];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [window.PDFView.document.nonsearchablePDF writeToURL:savePanel.URL];
            dispatch_async(dispatch_get_main_queue(), ^{
                [window finishExportingState];
            });
        });
    }
}
//
//- (void)saveMe {
//    [self.window finalizeRedactions];
//    NSSavePanel *savePanel = [[NSSavePanel alloc] init];
//    [savePanel setAllowedFileTypes:[NSArray arrayWithObject:@"pdf"]];
//    
//    if ([savePanel runModal] == YES) {
//        [self.window beginExportingState];
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//            [[AppDelegate nonsearchablePDFFromDocument:self.window.PDFView.document] writeToFile:savePanel.filename];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [self.window finishExportingState];
//            });
//        });
//    }
//}


- (IBAction)toggleFullscreen:(id)sender {
    [self.activeWindow toggleFullScreen:self];
}

- (void)windowDidEnterFullScreen:(NSNotification *)notification {
    self.menuItemFullscreen.title = @"Exit Full Screen";
}

- (void)windowDidExitFullScreen:(NSNotification *)notification {
    self.menuItemFullscreen.title = @"Enter Full Screen";
}

- (void)closeAllWindowsWithCompletionHandler:(void (^)(void))handler {
    RDWindow *window = self.windows.allObjects.lastObject;
    if (window) {
        __weak AppDelegate *weakself = self;
        [window closeWithCompletionHandler:^{
            if (weakself.windows.allObjects.lastObject) {
                [weakself closeAllWindowsWithCompletionHandler:handler];
            } else {
                handler();
            }
        }];
    } else {
        handler();
    }
}

- (void)addWindow:(RDWindow *)window {
    self.windows = [self.windows setByAddingObject:window];
}

- (void)removeWindow:(RDWindow *)windowToBeRemoved {
    NSMutableSet *windows = [[NSMutableSet alloc] init];
    for (RDWindow *window in self.windows) {
        if (window != windowToBeRemoved) {
            [windows addObject:window];
        }
    }
    self.windows = windows.copy;
}

- (RDWindow *)activeWindow {
    for (RDWindow *window in self.windows) {
        if (window.isKeyWindow) {
            return window;
        }
    }
    return nil;
}

- (NSSet *)windows {
    if (!_windows) {
        _windows = [[NSSet alloc] init];
    }
    return _windows;
}

- (RDWindow *)newWindow {
    RDWindow *window = [[RDWindow alloc] initWithNibNamed:@"RDWindow"];
    __weak AppDelegate *weakself = self;
    __weak RDWindow *weakwindow = window;
    window.willCloseHandler = ^{
        [weakself removeWindow:weakwindow];
    };
    return window;
}

@end
