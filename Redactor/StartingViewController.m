//
//  StartingViewController.m
//  Redactor
//
//  Created by William Palin on 4/13/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "StartingViewController.h"

@interface StartingViewController ()

@end

@implementation StartingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

- (IBAction)close:(id)sender {
    [self dismissViewController:self];
}
@end
