//
//  WindowController.h
//  Redactor
//
//  Created by William Palin on 4/8/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface WindowController : NSWindowController <NSWindowDelegate>

@end
