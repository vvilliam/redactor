//
//  PageResult.m
//  RedactPro
//
//  Created by William Palin on 3/13/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "Document.h"

@interface Document ()
@property (nonatomic) NSMutableArray *internalPages;
@end

@implementation Document

- (void)addPage:(Page *)page {
    [self.internalPages addObject:page];
}

- (NSArray *)pages {
    return self.internalPages.copy;
}

- (NSInteger)pageCount {
    return self.internalPages.count;
}

- (NSMutableArray *)internalPages {
    if (!_internalPages) {
        _internalPages = [[NSMutableArray alloc] init];
    }
    return _internalPages;
}

-(Page *)pageAtIndex:(NSInteger)index{
    for (Page *page in self.internalPages) {
        if (page.index == index) {
            return page;
        }
    }
    return nil;
}

@end
