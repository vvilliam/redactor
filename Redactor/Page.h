//
//  Page.h
//  RedactPro
//
//  Created by William Palin on 3/18/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Word.h"

typedef NSImage * (^ImageConstructor)(void);

@interface Page : NSObject

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, copy) NSImage *imageLarge;
@property (nonatomic, copy) NSString *blockText;
@property (nonatomic, copy) NSArray *recognizedSurnames;
@property (nonatomic, copy) NSArray *lines; // [Line]
@property (nonatomic, copy) NSArray *words; // [Word]
@property (nonatomic, copy) NSArray *surnames;
@property (nonatomic, copy) ImageConstructor image;
@property (nonatomic, copy) NSArray *justWords;

@property (nonatomic, copy) NSMutableArray *blackBars;
@property (nonatomic, assign) BOOL isPageCorrupted;
@property (nonatomic, copy) NSData *pageData;
@property (nonatomic, copy) NSImage *pageImage;

@property (nonatomic, copy) NSArray *phoneNumbers;
@property (nonatomic, copy) NSArray *addresses;
@property (nonatomic, copy) NSArray *date;
@property (nonatomic, copy) NSArray *ssns;

@property (nonatomic, copy) NSArray *definiteNames;

@end
