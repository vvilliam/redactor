//
//  LeftRightSplitView.h
//  Redactor
//
//  Created by William Palin on 4/8/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface LeftRightSplitView : NSSplitView <NSSplitViewDelegate>

@property (nonatomic, assign) BOOL isSelected;

@end
