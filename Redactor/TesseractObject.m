//
//  TesseractObject.m
//  TestWords
//
//  Created by William Palin on 3/13/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "TesseractObject.h"
#import "Page.h"
#import "Line.h"
#import "Word.h"
#import "NSImage+Filters.h"
#import "RegularPDF.h"

@implementation TesseractObject

+ (Document *)initPageDocument{
    Document *document;
    if(!document){
        document = [[Document alloc]init];
    };
    return document;
}

- (Document *)document{
    Document *document;
    if (!document) {
        document = [[Document alloc]init];
    }
    return document;
}

+ (Page *)pageFromPDF:(PDFDocument *)PDFDoc withIndex:(NSInteger)index{
    Page *page = [[Page alloc]init];
    
    page.blockText = [[PDFDoc pageAtIndex:index] string];
    page.index = index;
    page.words = [TesseractObject dataDetectionArray:page andEmptryMutableArray:[[NSMutableArray alloc]init]];
    page.blackBars = [RegularPDF createMutableArrayOfAllContentOnPage:[PDFDoc pageAtIndex:index] and:[[NSMutableArray alloc]init]];
    page.isPageCorrupted = [RegularPDF doesPageHaveHiddenContent:[PDFDoc pageAtIndex:index] andArrayOfContent:[[NSMutableArray alloc]init] and:[[NSMutableArray alloc]init]];
    
    return page;
}

+ (NSArray *)dataDetectionArray:(Page *)page andEmptryMutableArray:(NSMutableArray *)emptyArray {
    NSMutableArray *dataDetectionPhones = [TesseractObject dataDetectionPhoneNumbers:emptyArray andPage:page];
    NSMutableArray *dataDetectionAddress = [TesseractObject dataDetectionAddresses:emptyArray andPage:page];
    NSMutableArray *dataDetectionDates = [TesseractObject dataDetectionDates:emptyArray andPage:page];
    NSMutableArray *dataDetectionLinks = [TesseractObject dataDetectionLinks:emptyArray andPage:page];
    NSMutableArray *wordArray = [TesseractObject checkForMiddleInitial:[TesseractObject createWordObject:page]];
    
    page.definiteNames = [TesseractObject definiteNamesList:wordArray andPage:page];
    
    NSMutableSet *set = [NSMutableSet setWithArray:wordArray];
    [set addObjectsFromArray:dataDetectionPhones];
    [set addObjectsFromArray:dataDetectionAddress];
    [set addObjectsFromArray:dataDetectionDates];
    [set addObjectsFromArray:dataDetectionLinks];
    
    return [set allObjects];
}

+ (NSMutableArray *)definiteNamesList:(NSMutableArray *)wordArray andPage:(Page *)page {
    NSMutableArray *definiteWords = [[NSMutableArray alloc]init];
    for (NSInteger index = 1 ; index < [wordArray count]-1; index++) {
        Word *preWord = wordArray[index-1];
        Word *middleWord = wordArray[index];
        Word *postWord = wordArray[index+1];
        
        if (((preWord.isTest || postWord.isTest) && middleWord.isTest) || [preWord.textWithoutSymbols isEqualToString: @"Mr"] || [preWord.textWithoutSymbols isEqualToString:@"Mrs" ] || [preWord.textWithoutSymbols isEqualToString: @"Ms"] || [preWord.textWithoutSymbols isEqualToString: @"Dr"]) {
            [definiteWords addObject:middleWord.textWithoutSymbols];
        }
    }
    NSLog(@"the definite words are %@",definiteWords);
    return definiteWords;
}

+ (Page *)pageFromImage:(NSImage *(^)(void))image withIndex:(NSInteger)integer{
  
    Tesseract* tesseract = [[Tesseract alloc] initWithLanguage:@"eng"];
    [tesseract setVariableValue:[TesseractObject whiteList] forKey:@"tessedit_char_whitelist"];
    
    NSImage *imageX = image();
    NSMutableArray *arrayOfWordObjects = [[NSMutableArray alloc] init];

    [tesseract setImage:imageX];
    [tesseract setRect:CGRectMake(0, 0, imageX.size.width, imageX.size.height)];
    [tesseract recognize];
    
    NSArray *block = [tesseract getConfidenceByBlock];
    NSArray *wordX = [tesseract getConfidenceByWord];
    NSArray *lineX = [tesseract getConfidenceByTextline];
    
    if([block count] == 0){
        NSLog(@"skip it");
        Page *page = [[Page alloc]init];
        page.image = image;
        page.index = integer;
        page.pageImage = imageX;

        return page;
    }
    else {

        Page *page = [[Page alloc]init];
        page.image = image;
        page.blockText = [TesseractObject text:block][0];
        page.index = integer;
        page.addresses = [LinguisticTagger addresses:page.blockText];
        page.surnames = [LinguisticTagger lastNames:page.blockText];
        page.phoneNumbers = [LinguisticTagger phoneNumbers:page.blockText];
        page.date = [LinguisticTagger dates:page.blockText];
        page.ssns = [LinguisticTagger SSNs:page.blockText];
        page.justWords = [TesseractObject text:wordX];
        page.lines = [TesseractObject boundingbox:lineX];
        page.pageImage = imageX;
        
        for (NSInteger index = 0; index < wordX.count; index++) {
            Word *word = [[Word alloc]init];
            word.text = [TesseractObject text:wordX][index];
            word.index = integer;
            
            NSString *preWord;
            NSString *postWord;
            
            if ((index+1 < wordX.count) && !(index==0)) {
                preWord = [TesseractObject text:wordX][index-1];
                postWord = [TesseractObject text:wordX][index+1];
            }
            
            if(index+1 < wordX.count){
                word.coordinate = [TesseractObject convertRect:[TesseractObject createLinedRect:[TesseractObject boundingBoxRect:wordX atIndex:index] with:page.lines andNextRect:[TesseractObject boundingBoxRect:wordX atIndex:index+1]] withWidth:imageX.size.width andHeight:imageX.size.height andImageWidth:imageX.size.width andImageHeight:imageX.size.height];
                
                word.typeOfWord = [TesseractObject determineTypeOfWord:page andWord:word withPreviousWord:preWord andPostWord:postWord];
            }
            else {
                word.coordinate = [TesseractObject convertRect:[TesseractObject createLinedRect:[TesseractObject boundingBoxRect:wordX atIndex:index] with:page.lines andNextRect:[TesseractObject boundingBoxRect:wordX atIndex:index]] withWidth:imageX.size.width andHeight:imageX.size.height andImageWidth:imageX.size.width andImageHeight:imageX.size.height];
            }
             [arrayOfWordObjects addObject:word];
        }
        page.words = arrayOfWordObjects;
        
    return page;
    }
}

+ (NSString *)whiteList {
    return @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789[](),';.-_&%$@#/?!:. *\"";
}

+ (NSString *)determineTypeOfWord:(Page *)page andWord:(Word *)word withPreviousWord:(NSString *)preWord andPostWord:(NSString *)postWord {
    
    //previous and post word are used in different ways on differnt checks.  for addresses - addresses must come in pairs to be recorded.  this removes false positives
    if([page.addresses containsObject:word.text] || [page.addresses containsObject:word.textWithoutSymbols]) {
        if([page.addresses containsObject:preWord] || [page.addresses containsObject:postWord]) {
            return @"address";
        }
    }
    if([page.surnames containsObject:word.textWithoutSymbols]) {
        return @"name"; }
    //here i use pre and post words to check for middle initials- if the previous and post words are both names - check the structure of the current word.  i check to see if its a single alphabetic character after removing non-alpha characters
    if([page.surnames containsObject:[TesseractObject textWithoutSymbols:preWord]] && [page.surnames containsObject:[TesseractObject textWithoutSymbols:postWord]] && [TesseractObject isMiddleInitial:word.text]){
        return @"name"; }
    
    if([page.date containsObject:word.text] || [page.date containsObject:word.textWithoutSymbols]) {
        if([page.date containsObject:preWord] || [page.date containsObject:postWord]) {
            return @"date"; }
    }
    
    if([page.phoneNumbers containsObject:word.text] || [page.phoneNumbers containsObject:word.textWithoutSymbols]) {
        return @"phone"; }
    
    if([page.ssns containsObject:word.noPunctuation]) {
        return @"ssn"; }

    return nil;
}

+ (NSString *)textWithoutSymbols:(NSString *)text{
    NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    NSString *textWithoutSymbols = [[text componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    return textWithoutSymbols;
}

+ (BOOL)isMiddleInitial:(NSString *)text{
    NSCharacterSet *charactersToRemove = [[NSCharacterSet letterCharacterSet] invertedSet];
    NSString *onlyText = [[text componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    [onlyText stringByTrimmingCharactersInSet:charactersToRemove];
    BOOL yesNo = (onlyText.length == 1) && (text.length ==2);
    
    return yesNo;
}



+ (NSMutableArray *)text:(NSArray *)tessData{
    NSMutableArray *textArray = [[NSMutableArray alloc]init];
    for (NSDictionary *confidence in tessData) {
        [textArray addObject: confidence[@"text"]];
    }
    return textArray;
}

+ (NSMutableArray *)boundingbox:(NSArray *)tessData{
    NSMutableArray *textArray = [[NSMutableArray alloc]init];
    for (NSDictionary *confidence in tessData) {
        [textArray addObject: confidence[@"boundingbox"]];
    }
    return textArray;
}

+ (NSRect)boundingBoxRect:(NSArray *)tessData atIndex:(NSInteger)index {
    NSMutableArray *textArray = [[NSMutableArray alloc]init];
    for (NSDictionary *confidence in tessData) {
        [textArray addObject: confidence[@"boundingbox"]];
    }
    return NSRectFromString([NSString stringWithFormat:@"%@",textArray[index]]);
}

+ (NSMutableArray *)confidence:(NSArray *)tessData{
    NSMutableArray *textArray = [[NSMutableArray alloc]init];
    for (NSDictionary *confidence in tessData) {
        [textArray addObject: confidence[@"confidence"]];
    }
    return textArray;
}

+ (NSRect)createLinedRect:(NSRect)rect with:(NSArray *)lines andNextRect:(NSRect)nextRect {
    NSRect newRect;
    for (NSInteger index = 0; index < [lines count]; index++) {
        
        float ax = nextRect.origin.x;
        float x = rect.origin.x;
        
        
        NSRect lineRect = NSRectFromString([NSString stringWithFormat:@"%@",lines[index]]);
        if((lineRect.origin.y-(15)) < rect.origin.y && rect.origin.y < (lineRect.origin.y+lineRect.size.height)) {
            NSInteger num;
            NSInteger xF;
            if((ax-x >0) && (ax-x > rect.size.width)){
                num = (ax-x)-5;
                
                CGFloat floaty = num/rect.size.width;
                if(floaty > 2){
                    num = rect.size.width *1.5;
                }
                else {
                    if ([[NSScreen mainScreen] backingScaleFactor]==1) {
                        xF = 2;
                    }
                    else {
                        xF = 5;
                    }
                    num = (ax-x)-xF;
                }
            }
            else {
                num = rect.size.width + 5;
                
            }
            newRect = NSMakeRect(rect.origin.x, lineRect.origin.y-1, num, lineRect.size.height+1);
        }
    }
    return newRect;
}



+ (NSRect)convertRect:(NSRect)rect withWidth:(CGFloat)width andHeight:(CGFloat)height andImageWidth:(CGFloat)imageWidth andImageHeight:(CGFloat)imageHeight {
    CGFloat factor = [[NSScreen mainScreen] backingScaleFactor];
    
    float xPoint = ((rect.origin.x-2/2)+(((width/2)-(width/2))/2)-2)/2;
    float yPoint = ((((height)-((rect.origin.y)-1))-((rect.size.height)+2))/2);
    
    return NSMakeRect(xPoint, yPoint, ((rect.size.width)+3*factor)/2, ((rect.size.height)+2)/2);
}




+ (NSMutableArray *)createWordObject:(Page *)pageAtIndex {
    NSLinguisticTaggerOptions options = NSLinguisticTaggerOmitWhitespace | NSLinguisticTaggerOmitPunctuation ;
    NSLinguisticTagger *tagger = [[NSLinguisticTagger alloc] initWithTagSchemes: [NSLinguisticTagger availableTagSchemesForLanguage:@"en"] options:options];
    tagger.string = pageAtIndex.blockText;
    NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
    
    
    [tagger enumerateTagsInRange:NSMakeRange(0, [pageAtIndex.blockText length]) scheme:NSLinguisticTagSchemeLexicalClass options:options usingBlock:^(NSString *tag, NSRange tokenRange, NSRange sentenceRange, BOOL *stop) {
        
        NSString *token = [tagger.string substringWithRange:tokenRange];
        BOOL isUppercase = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:[token characterAtIndex:0]];
        
        Word *word = [[Word alloc]init];
        word.text = token;
        word.rangeForWords = tokenRange;
        word.partOfSpeech = tag;
        word.capitalized = isUppercase;
        
        if(([tag isEqualTo:@"Noun"] || [tag isEqualTo:@"OtherWord"]) && isUppercase){
            word.noun = TRUE;
            word.isTest = [TesseractObject areWeAName:word.text];
            
        }

        
        [mutableArray addObject:word];
    }];
    

    
    return mutableArray;
}

+ (NSMutableArray *)checkForMiddleInitial:(NSMutableArray *)arrayOfWords {
 
    for (NSInteger index = 1 ; index < [arrayOfWords count]-1; index++) {
        Word *preWord = arrayOfWords[index-1];
        Word *middleWord = arrayOfWords[index];
        Word *postWord = arrayOfWords[index+1];
        
        if ((preWord.isTest && postWord.isTest) && [TesseractObject isMiddleInitial:middleWord.text]) {
            middleWord.isTest = TRUE;
            middleWord.rangeForWords = NSMakeRange(preWord.rangeForWords.location+preWord.rangeForWords.length, 2);
        }
    }
    return arrayOfWords;
}

+ (NSMutableArray *)dataDetectionPhoneNumbers:(NSMutableArray *)mutableArray andPage:(Page *)pageAtIndex {
    NSError *error = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber
                                                               error:&error];
    [detector enumerateMatchesInString:pageAtIndex.blockText
                               options:kNilOptions
                                 range:NSMakeRange(0, [pageAtIndex.blockText length])
                            usingBlock:
     ^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
         
         NSString *stringResult = [pageAtIndex.blockText substringWithRange:result.range];

         if ([[stringResult substringWithRange:NSMakeRange(3, 1)] isEqualToString: @"-" ] && [[stringResult substringWithRange:NSMakeRange(6, 1)] isEqualToString: @"-" ]) {

             Word *word = [[Word alloc]init];
             word.isSSN = YES;
             word.rangeForWords = result.range;
             [mutableArray addObject:word];
         }
         else {
             
             Word *word = [[Word alloc]init];
             word.isPhoneNumber = YES;
             word.rangeForWords = result.range;
             [mutableArray addObject:word];
         }
     }];
    return mutableArray;
}

+ (NSMutableArray *)dataDetectionAddresses:(NSMutableArray *)mutableArray andPage:(Page *)pageAtIndex {
    NSError *error = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeAddress
                                                               error:&error];
    [detector enumerateMatchesInString:pageAtIndex.blockText
                               options:kNilOptions
                                 range:NSMakeRange(0, [pageAtIndex.blockText length])
                            usingBlock:
     ^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
         Word *word = [[Word alloc]init];
         word.isAddress = YES;
         word.rangeForWords = result.range;
         
         [mutableArray addObject:word];
     }];
    return mutableArray;
}

+ (NSMutableArray *)dataDetectionDates:(NSMutableArray *)mutableArray andPage:(Page *)pageAtIndex {
    NSError *error = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeDate
                                                               error:&error];
    [detector enumerateMatchesInString:pageAtIndex.blockText
                               options:kNilOptions
                                 range:NSMakeRange(0, [pageAtIndex.blockText length])
                            usingBlock:
     ^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
         Word *word = [[Word alloc]init];
         word.isDate = YES;
         word.rangeForWords = result.range;
         
         [mutableArray addObject:word];
     }];
    return mutableArray;
}

+ (NSMutableArray *)dataDetectionLinks:(NSMutableArray *)mutableArray andPage:(Page *)pageAtIndex {
    NSError *error = nil;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                               error:&error];
    [detector enumerateMatchesInString:pageAtIndex.blockText
                               options:kNilOptions
                                 range:NSMakeRange(0, [pageAtIndex.blockText length])
                            usingBlock:
     ^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
         Word *word = [[Word alloc]init];
         word.isLink = YES;
         word.rangeForWords = result.range;
         
         [mutableArray addObject:word];
     }];
    return mutableArray;
}

+ (BOOL)areWeAName:(NSString *)token{
    BOOL yn;
    
    BOOL contains =  [[TesseractObject commonLastNamesStatic] containsObject:[token uppercaseString]];
    BOOL contains2 = [[TesseractObject commonNounsStatic] containsObject:[token lowercaseString]];
    BOOL contains3 = [[TesseractObject commonFirstNamesStatic] containsObject:[token uppercaseString]];
    
    if (contains2) {
        yn = NO;
    }
    else if (contains || contains3){
        yn = YES;
    }
    else {
        yn = NO;
    }
    
    return yn;
}


+ (NSArray *)commonNounsStatic {
    static NSArray *array;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        array = [TesseractObject commonNouns];
    });
    return array;
}

+ (NSArray *)commonLastNamesStatic {
    static NSArray *array;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        array = [TesseractObject commonLastNames];
    });
    return array;
}

+ (NSArray *)commonFirstNamesStatic {
    static NSArray *array;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        array = [TesseractObject commonFirstNames];
    });
    return array;
}

+ (NSArray *)commonNouns{
    NSString *cWords = [[NSBundle mainBundle] pathForResource:@"commonWords" ofType:@"csv"];
    NSString *wordData = [NSString stringWithContentsOfFile:cWords encoding:NSUTF8StringEncoding error:nil];
    NSMutableCharacterSet *cwordsKeep = [NSMutableCharacterSet alphanumericCharacterSet];
    [cwordsKeep addCharactersInString:@","];
    NSCharacterSet *cRemove = [cwordsKeep invertedSet];
    NSString *tReplace = [[wordData componentsSeparatedByCharactersInSet:cRemove] componentsJoinedByString:@""];
    NSArray* cRows = [tReplace componentsSeparatedByString:@","];
    
    return cRows;
}

+ (NSArray *)commonLastNames{
    NSString *rdDP = [[NSBundle mainBundle] pathForResource:@"LastNames" ofType:@"csv"];
    NSString *dataStr = [NSString stringWithContentsOfFile:rdDP encoding:NSUTF8StringEncoding error:nil];
    NSMutableCharacterSet *charactersToKeep = [NSMutableCharacterSet alphanumericCharacterSet];
    [charactersToKeep addCharactersInString:@","];
    NSCharacterSet *charactersToRemove = [charactersToKeep invertedSet];
    NSString *trimmedReplacement = [[dataStr componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    NSArray* rows = [trimmedReplacement componentsSeparatedByString:@","];
    return rows;
}

+ (NSArray *)commonFirstNames {
    NSMutableCharacterSet *charactersToKeep = [NSMutableCharacterSet alphanumericCharacterSet];
    NSCharacterSet *charactersToRemove = [charactersToKeep invertedSet];
    NSString *fNames = [[NSBundle mainBundle] pathForResource:@"listOfNamesNoSpace" ofType:@"csv"];
    NSString *fString = [NSString stringWithContentsOfFile:fNames encoding:NSUTF8StringEncoding error:nil];
    NSMutableCharacterSet *fWordsToKeep = [NSMutableCharacterSet alphanumericCharacterSet];
    [fWordsToKeep addCharactersInString:@","];
    NSString *tRstring = [[fString componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    return [tRstring componentsSeparatedByString:@","];
}

@end
