//
//  MakeImageFromPdfPage.h
//  Redactor
//
//  Created by William Palin on 4/8/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MakeImageFromPdfPage : NSObject

+ (NSSize)giveMeSize:(NSString *)filePath;
+ (NSImage *)blankImageWithSize:(CGSize)size;
+ (NSImage *)blackImageWithSize:(CGSize)size;

@end
