//
//  MarkupSplitView.m
//  Redactor
//
//  Created by William Palin on 4/8/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "MarkupSplitView.h"

@implementation MarkupSplitView

- (instancetype)initWithCoder:(NSCoder *)coder{
    self = [super initWithCoder:coder];
    if (self){
        self.delegate = self;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(toggleSplitView)
                                                     name:@"toggleMarkup"
                                                   object:nil];
        
        [self setPosition:-10 ofDividerAtIndex:0];        
    }
    
    
    return self;
}

-(CGFloat)splitView:(NSSplitView *)splitView constrainMaxCoordinate:(CGFloat)proposedMaximumPosition ofSubviewAt:(NSInteger)dividerIndex{
    return 35;
}


- (void)toggleSplitView{
    self.isSelected = !self.isSelected;  // toggle method
    if (self.isSelected){
        [self setPosition:35 ofDividerAtIndex:0];
        
    }else{
        [self setPosition:0 ofDividerAtIndex:0];
    }
}


@end
