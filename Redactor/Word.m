//
//  Word.m
//  RedactPro
//
//  Created by William Palin on 3/18/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "Word.h"

@interface Word ()

@property (nonatomic) NSHashTable *selectionHandlers;

@end

@implementation Word

- (void)addSelectionHandler:(void (^)(Word *word))selectionHandler {
    [self.selectionHandlers addObject:selectionHandler];
}

- (void)toggleSelection{
    self.isSelected = !self.isSelected;  // toggle method
    for (void (^completionHandler)(Word *word) in self.selectionHandlers) {
        completionHandler(self);
    }

    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"updateTable"
     object:nil];
}

- (NSString *)textWithoutSymbols {
    return [self textWithoutSymbols:self.text];
}

- (NSString *)textWithoutSymbols:(NSString *)text{
    NSCharacterSet *charactersToRemove = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    NSString *textWithoutSymbols = [[text componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    return textWithoutSymbols;
}

- (NSString *)noPunctuation {
    return [self removedPunctuation:self.text];
}


- (NSString *)removedPunctuation:(NSString *)text{
    NSMutableCharacterSet *characterSetX = [NSMutableCharacterSet alphanumericCharacterSet];
    [characterSetX addCharactersInString:@"-"];
    NSCharacterSet *charactersToRemove = [characterSetX invertedSet];
    NSString *textWithoutSymbols = [[text componentsSeparatedByCharactersInSet:charactersToRemove] componentsJoinedByString:@""];
    return textWithoutSymbols;
}


- (NSHashTable *)selectionHandlers {
    if (!_selectionHandlers) {
        _selectionHandlers = [[NSHashTable alloc] initWithOptions:NSPointerFunctionsWeakMemory capacity:0];
    }
    return _selectionHandlers;
}

@end
