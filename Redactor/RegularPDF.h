//
//  RegularPDF.h
//  Redactor
//
//  Created by William Palin on 4/17/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <Quartz/Quartz.h>

@interface RegularPDF : NSObject

+ (BOOL)doesPageHaveHiddenContent:(PDFPage *)pageAtIndex andArrayOfContent:(NSMutableArray *)opaqueContent and:(NSMutableArray *)allContent;
+ (NSMutableArray *)createMutableArrayOfAllContentOnPage:(PDFPage *)pageAtIndex and:(NSMutableArray *)allVisibleContent;

@end
