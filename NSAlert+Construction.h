//
//  NSAlert+Construction.h
//  
//
//  Created by Brian on 6/14/15.
//
//

@import Cocoa;

@interface NSAlert (Construction)

+ (NSAlert *)alertForFormatSelection;

@end
