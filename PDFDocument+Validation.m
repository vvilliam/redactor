//
//  PDFDocument+Validation.m
//  
//
//  Created by Brian on 6/13/15.
//
//

#import "PDFDocument+Validation.h"

@implementation PDFDocument (Validation)

- (BOOL)isText {
    PDFPage *firstPage = [self pageAtIndex:0];
    NSCharacterSet *set = [NSCharacterSet alphanumericCharacterSet];
    NSRange alphanumeric = [firstPage.string rangeOfCharacterFromSet:set];
    return alphanumeric.location != NSNotFound;
}

@end
