//
//  PDFDocument+Construction.h
//  
//
//  Created by Brian on 6/14/15.
//
//

@import Quartz;

@interface PDFDocument (Construction)

+ (PDFDocument *)documentFromFileWithURL:(NSURL *)URL;

@end
