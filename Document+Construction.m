//
//  Document+Construction.m
//  Redactor
//
//  Created by William Palin on 6/1/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "Document+Construction.h"
#import "TesseractObject.h"
#import "MakeImageFromPdfPage.h"
#import "PdfSelections.h"

@implementation Document (Construction)

+ (void)documentFromPDFDocumentConstructor:(PDFDocument *(^)(void))PDFDocumentConstructor
                     withCompletionHandler:(void (^)(Document *document))completionHandler {
    
    Document *document = [[Document alloc] init];
    document.PDFDocument = PDFDocumentConstructor;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self parsePDFDocumentConstructor:PDFDocumentConstructor
          withUpdateHandlerPDF:^(Page *page, CGFloat progress) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [document addPage:page];
            });
        } andCompletionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(document);
            });
        }];
    });    
}

+ (void)parsePDFDocumentConstructor:(PDFDocument *(^)(void))PDFDocumentConstructor
               withUpdateHandlerPDF:(void (^)(Page *page, CGFloat progress))updateHandler
               andCompletionHandler:(void (^)(void))completionHandler {
    
    __block NSInteger progressCount = 0;
    PDFDocument *document = PDFDocumentConstructor();
    NSInteger pageCount = document.pageCount;
    
    for (NSInteger index = 0; index < pageCount; index++) {
        /* Each iteration needs it's own PDFDocument because the operations are dispatched asynchronously. Asynchronous operations should not share resources. Thus the recreation of PDFDocument below. */
        PDFDocument *PDFDocument = PDFDocumentConstructor();
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                updateHandler([TesseractObject pageFromPDF:PDFDocument withIndex:index], (CGFloat)(progressCount + 1)/(CGFloat)pageCount);
                progressCount++;
                if (progressCount == pageCount) {
                    completionHandler();
                }
            }
        });
    }
}

+ (void)imageDocumentFromPDFDocumentConstructor:(PDFDocument *(^)(void))PDFDocumentConstructor
                              completionHandler:(void (^)(Document *document))completionHandler {
    
    Document *document = [[Document alloc] init];
    document.PDFDocument = PDFDocumentConstructor;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self imageDocumentFromPDFDocumentConstructor:PDFDocumentConstructor
                         withUpdateHandler:^(Page *page, CGFloat progress) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [document addPage:page];
            });
        } andCompletionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(document);
            });
        }];
    });
}

+ (void)imageDocumentFromPDFDocumentConstructor:(PDFDocument *(^)(void))PDFDocumentConstructor
                              withUpdateHandler:(void (^)(Page *page, CGFloat progress))updateHandler
                           andCompletionHandler:(void (^)(void))completionHandler {
    
    __block NSInteger progressCount = 0;
    PDFDocument *document = PDFDocumentConstructor();
    NSInteger pageCount = document.pageCount;
    
    for (NSInteger index = 0; index < pageCount; index++) {
        /* Each iteration needs it's own PDFDocument because the operations are dispatched asynchronously. Asynchronous operations should not share resources. Thus the recreation of PDFDocument below. */
        PDFDocument *PDFDocument = PDFDocumentConstructor();
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                updateHandler([TesseractObject pageFromImage:^{
                    PDFPage *page = [PDFDocument pageAtIndex:index];
                    NSImage *image = [self imageFromPDFPage:page];
                    return image;
                } withIndex:index], (CGFloat)(progressCount + 1)/(CGFloat)pageCount);
                progressCount++;
                if (progressCount == pageCount) {
                    completionHandler();
                }
            }
        });
    }
}

+ (NSImage *)imageFromPDFPage:(PDFPage *)page {
    @synchronized(self)
    {
        return [self imageFromPDFData:page.dataRepresentation];
    }
}

+ (NSImage *)imageFromPDFData:(NSData *)PDFData {
    NSPDFImageRep* pdfImageRep = [NSPDFImageRep imageRepWithData:PDFData];
    NSImage* scaledImage = [NSImage imageWithSize:NSMakeSize(pdfImageRep.bounds.size.width*2, pdfImageRep.bounds.size.height*2) flipped:NO drawingHandler:^BOOL(NSRect dstRect){
        [pdfImageRep drawInRect:dstRect];
        return YES;
    }];
    return [[NSImage alloc] initWithData:[[NSBitmapImageRep imageRepWithData:[scaledImage TIFFRepresentation]] representationUsingType:NSJPEGFileType properties:nil]];
}



- (NSData *)searchablePDF {
    for (NSInteger index = 0; index < self.PDFDocument().pageCount; index++) {
        [self pageAtIndex:index].pageData = [self generatePage:self.PDFDocument() atIndex:index];
    }
    
    PDFDocument *anotherDoc = [self anotherDoc:[self pageAtIndex:0].pageData];
    for (NSInteger index = 1; index < self.PDFDocument().pageCount; index++) {
        [anotherDoc insertPage:[[self anotherDoc:[self pageAtIndex:index].pageData] pageAtIndex:0] atIndex:index];
    }
    return anotherDoc.dataRepresentation;
}

- (PDFDocument *)anotherDoc:(NSData *)data {
    return [[PDFDocument alloc]initWithData:data];
}

- (NSData *)generatePage:(PDFDocument *)PDFDocumentX atIndex:(NSInteger)index {
    NSData *data;
    PDFPage *PDFPageX = [PDFDocumentX pageAtIndex:index];
    
    if ([self isTextPage:PDFPageX]) {
        
        CFMutableDataRef mutableData = CFDataCreateMutable(NULL, 0);
        NSImage *image = [[NSImage alloc] initWithData:[Document imageDataForPage:[PDFDocumentX pageAtIndex:index]]];
        NSRect rect = NSMakeRect(0, 0, image.size.width, image.size.height);
        CGContextRef pdfContext = CGPDFContextCreate(CGDataConsumerCreateWithCFData(mutableData), &rect, NULL);
        CFRelease(CGDataConsumerCreateWithCFData(mutableData));
        
        Page *page = [self pageAtIndex:index];
        CGImageSourceRef source = CGImageSourceCreateWithData((CFDataRef)[Document imageDataForPage:[PDFDocumentX pageAtIndex:index]], NULL);
        
        CGSize size = CGPDFPageGetBoxRect(PDFPageX.pageRef, kCGPDFCropBox).size;
        CGRect mediaRect = CGRectMake(0, 0, size.width, size.height);
        CGContextBeginPage(pdfContext, &mediaRect);
        
        CGImageRef imageRef = CGImageSourceCreateImageAtIndex(source, 0, NULL);
        CFRelease(source);
        CGContextDrawImage(pdfContext, CGRectMake(0, 0, image.size.width, image.size.height), imageRef);
        CFRelease(imageRef);
        
        for (NSInteger index = 0; index < page.blackBars.count; index++) {
            NSRect rect = [page.blackBars[index] boundsForPage:PDFPageX];
            
            if ([[page.blackBars[index] string] rangeOfCharacterFromSet:[Document characterSets]].location != NSNotFound && !([[page.blackBars[index] string] isEqual: @"+"]) && !([PDFPageX annotationAtPoint:[PdfSelections averagePoint:rect]])){
                if ([page.blackBars[index] attributedString]){
                    CTLineRef displayLine = CTLineCreateWithAttributedString((CFAttributedStringRef)[page.blackBars[index] attributedString]);
                    CGContextSetTextPosition(pdfContext, rect.origin.x, rect.origin.y+2.5);
                    CTLineDraw(displayLine, pdfContext);
                    CFRelease(displayLine );
                }
            }
    }
    CGContextEndPage(pdfContext);
    
    data = (__bridge NSData *)(mutableData);
    CFRelease(pdfContext);
    CFRelease(mutableData);
    }
    else {
        NSImage *rasterizedPage = [self imageFromPDFPage:PDFPageX];
        PDFPage *newPage = [[PDFPage alloc]initWithImage:rasterizedPage];
        return newPage.dataRepresentation;
    }
    
    return data;
}
//forgive me for bringing in this as an instance variable -
- (NSImage *)imageFromPDFPage:(PDFPage *)page {
    NSImage *image = [[NSImage alloc] initWithSize:CGPDFPageGetBoxRect(page.pageRef, kPDFDisplayBoxCropBox).size];
    [image lockFocus];
    [page drawWithBox:kPDFDisplayBoxCropBox];
    [image unlockFocus];
    return image;
}
//this checks the page if its an image or not - and draws it instead of writes it
- (BOOL)isTextPage:(PDFPage *)firstPage {
    NSCharacterSet *set = [NSCharacterSet alphanumericCharacterSet];
    NSRange alphanumeric = [firstPage.string rangeOfCharacterFromSet:set];
    return alphanumeric.location != NSNotFound;
}


+ (NSData *)imageDataForPage:(PDFPage *)page {
    CGSize size = CGPDFPageGetBoxRect(page.pageRef, kCGPDFMediaBox).size;
    NSImage *image = [MakeImageFromPdfPage blankImageWithSize:CGSizeMake(size.width, size.height)];
    for (PDFAnnotation *annotation in page.annotations) {
        [image lockFocus];
        [[MakeImageFromPdfPage blackImageWithSize:annotation.bounds.size] drawInRect:annotation.bounds];
        [image unlockFocus];
    }
    return image.TIFFRepresentation;
}

+ (NSCharacterSet *)characterSets {
    NSCharacterSet *charactersToCheck = [NSCharacterSet characterSetWithCharactersInString:@"\"abcdefghijk§lmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123+4567890_.,*&@#$%'?!-)([]:/|"];
    return charactersToCheck;
}

@end
