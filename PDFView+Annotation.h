//
//  PDFView+Annotation.h
//  Redactor
//
//  Created by Brian on 5/30/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Quartz/Quartz.h>
#import "Document.h"

@interface PDFView (Annotation)

- (void)redactCurrentPage;
- (void)redactHighlights;
- (void)redactSelectedWordsForDocument:(Document *)document;
- (void)unredactSelectedWordsForDocument:(Document *)document;
- (void)redactNamesFromDocument:(Document *)document;
- (void)unredactNamesFromDocument:(Document *)document;
- (void)removeAllAnnotations;
- (NSArray *)nameWidgets;
- (NSArray *)addressWidgets;
- (NSArray *)phoneWidgets;
- (NSArray *)dateWidgets;
- (NSArray *)linkWidgets;
- (NSArray *)ssnWidgets;


+ (PDFAnnotationButtonWidget *)annotation:(NSString *)withFieldName andLocation:(NSRect)rect;

- (void)redactDatesFromDocument:(Document *)document;
- (void)unredactDatesFromDocument:(Document *)document;

- (void)redactAddressesFromDocument:(Document *)document;
- (void)unredactAddressesFromDocument:(Document *)document;

- (void)redactPhonesFromDocument:(Document *)document;
- (void)unredactPhonesFromDocument:(Document *)document;

- (void)redactSSNsFromDocument:(Document *)document;
- (void)unredactSSNsFromDocument:(Document *)document;

- (void)redactLinksFromDocument:(Document *)document;
- (void)unredactLinksFromDocument:(Document *)document;


@end
