//
//  Document+Construction.h
//  Redactor
//
//  Created by William Palin on 6/1/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

@import Quartz;

#import "Document.h"

@interface Document (Construction)

+ (void)documentFromPDFDocumentConstructor:(PDFDocument *(^)(void))PDFDocumentConstructor
          withCompletionHandler:(void (^)(Document *document))completionHandler;

+ (void)imageDocumentFromPDFDocumentConstructor:(PDFDocument *(^)(void))PDFDocumentConstructor
                   completionHandler:(void (^)(Document *document))completionHandler;

- (NSData *)searchablePDF;

@end
