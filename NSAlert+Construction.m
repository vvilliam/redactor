//
//  NSAlert+Construction.m
//  
//
//  Created by Brian on 6/14/15.
//
//

#import "NSAlert+Construction.h"

@implementation NSAlert (Construction)

+ (NSAlert *)alertForFormatSelection {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"Searchable PDF"];
    [alert addButtonWithTitle:@"Non-Searchable PDF"];
    [alert setMessageText:@"Create PDF"];
    [alert setInformativeText:@"Searchable PDF is in beta and does not support tables, images, and colors."];
    [alert setAlertStyle:NSInformationalAlertStyle];
    return alert;
}

@end
