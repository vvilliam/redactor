//
//  PDFDocument+Construction.m
//  
//
//  Created by Brian on 6/14/15.
//
//

#import "PDFDocument+Construction.h"

@implementation PDFDocument (Construction)

+ (PDFDocument *)documentFromFileWithURL:(NSURL *)URL {
    NSData *data = [[NSData alloc] initWithContentsOfURL:URL];
    PDFDocument *document = [[PDFDocument alloc] initWithData:data];
    return document;
}

@end
