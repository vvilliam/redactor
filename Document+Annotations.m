//
//  Document+Annotations.m
//  Redactor
//
//  Created by Brian on 5/30/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "Document+Annotations.h"

@implementation Document (Annotations)

//- (NSArray *)annotations {
//    NSMutableArray *annotations = [[NSMutableArray alloc] init];
//    for (NSUInteger index = 0; index < self.pageCount; index++) {
//        for (Word *word in [self pageAtIndex:index].words) {
//            RDAnnotation *annotation = [[RDAnnotation alloc] initWithPageNumber:index andWidget:[self annotation:[self annoLabel:word.isSelected] andLocation:[self checkForPdfError:word.coordinate]]];
//            [annotations addObject:annotation];
//        }
//    }
//    return annotations;
//}

//- (NSString *)annoLabel:(BOOL)selected {
//    return selected ? @"names" : @"blue";
//}

//- (NSRect)checkForPdfError:(NSRect)rect{
//    //this code deals with the occasional PDF that is rotated sideways in the meta data
//    NSRect annotationBounds;
//    NSRect pageBounds = [self.pdfView.currentPage boundsForBox:self.pdfView.displayBox];
//    if (pageBounds.size.height < pageBounds.size.width) {
//        return NSMakeRect(
//            rect.origin.y,
//            (612- rect.origin.x)-(rect.size.width),
//            rect.size.height,
//            rect.size.width
//        );
//    } else {
//        return rect;
//    }
//}

@end
