//
//  RDAnnotation.h
//  Redactor
//
//  Created by Brian on 5/23/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Foundation/Foundation.h>
@import Quartz;

@interface RDAnnotation : NSObject

- (instancetype)initWithPageNumber:(NSUInteger)pageNumber andWidget:(PDFAnnotationButtonWidget *)widget;
- (NSUInteger)pageNumber;
- (PDFAnnotationButtonWidget *)widget;

@end
