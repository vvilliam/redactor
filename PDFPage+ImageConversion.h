//
//  PDFPage+ImageConversion.h
//  
//
//  Created by Brian on 6/14/15.
//
//

@import Quartz;

@interface PDFPage (ImageConversion)

+ (NSImage *)imageFromPDFPage:(PDFPage *)page;

@end
