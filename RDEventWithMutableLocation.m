//
//  RDEventWithMutableLocation.m
//  Redactor
//
//  Created by William Palin on 6/16/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "RDEventWithMutableLocation.h"

@interface RDEventWithMutableLocation ()
@property (nonatomic, assign) NSPoint internalPoint;
@end

@implementation RDEventWithMutableLocation

- (instancetype)initWithWindowLocation:(NSPoint)location {
    self = [super init];
    if (self) {
        self.internalPoint = location;
    }
    return self;
}

- (NSPoint)locationInWindow {
    return self.internalPoint;
}

@end
