//
//  RDAnnotation.m
//  Redactor
//
//  Created by Brian on 5/23/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "RDAnnotation.h"

@interface RDAnnotation ()

@property (nonatomic, assign) NSUInteger pageNumber;
@property (nonatomic) PDFAnnotationButtonWidget *widget;

@end

@implementation RDAnnotation

- (instancetype)initWithPageNumber:(NSUInteger)pageNumber andWidget:(PDFAnnotationButtonWidget *)widget {
    self = [super init];
    if (self) {
        self.pageNumber = pageNumber;
        self.widget = widget;
    }
    return self;
}

@end
