//
//  PDFView+Validation.h
//  
//
//  Created by Brian on 6/15/15.
//
//

@import Quartz;

@interface PDFView (Validation)

- (BOOL)isPossiblySearchable;

@end
