//
//  PDFDocument+Rasterization.m
//  
//
//  Created by Brian on 6/14/15.
//
//

#import "PDFDocument+Rasterization.h"
#import "PDFPage+ImageConversion.h"

@implementation PDFDocument (Rasterization)

- (PDFDocument *)nonsearchablePDF {
    PDFDocument *pdf = [[PDFDocument alloc] init];
    for (NSUInteger index = 0; index < self.pageCount; index++) {
        PDFPage *page = [self pageAtIndex:index];
        NSImage *rasterizedPage = [PDFPage imageFromPDFPage:page];
        PDFPage *newPage = [[PDFPage alloc]initWithImage:rasterizedPage];
        [pdf insertPage:newPage atIndex:index];
    };
    return pdf;
}

@end
