//
//  PDFDocument+Rasterization.h
//  
//
//  Created by Brian on 6/14/15.
//
//

@import Quartz;

@interface PDFDocument (Rasterization)

- (PDFDocument *)nonsearchablePDF;

@end
