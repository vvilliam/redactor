//
//  PDFPage+Annotation.h
//  Redactor
//
//  Created by Brian on 5/30/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

@import Quartz;

@interface PDFPage (Annotation)

- (void)redact;

@end
