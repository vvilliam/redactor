//
//  RDEventWithMutableLocation.h
//  Redactor
//
//  Created by William Palin on 6/16/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface RDEventWithMutableLocation : NSEvent

- (instancetype)initWithWindowLocation:(NSPoint)location;

@end
