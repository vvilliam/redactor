//
//  PDFView+Annotation.m
//  Redactor
//
//  Created by Brian on 5/30/15.
//  Copyright (c) 2015 William Palin. All rights reserved.
//

#import "PDFView+Annotation.h"
#import "PdfSelections.h"
#import "RDAnnotation.h"

@implementation PDFView (Annotation)

- (void)removeAllAnnotations {
    for (NSInteger index = 0; index < self.document.pageCount; index++) {
        PDFPage *activePage = [self.document pageAtIndex:index];
        for (NSInteger index = 0; index < activePage.annotations.count; index++) {
            [activePage removeAnnotation:activePage.annotations[index]];
        }
    }
}

- (NSArray *)nameWidgets {
    NSMutableArray *nameAnnotations = [[NSMutableArray alloc] init];
    for (NSInteger pageCount = 0; pageCount < self.document.pageCount; pageCount++) {
        NSArray *annotations = [[self.document pageAtIndex:pageCount] annotations];
        for (NSInteger index = 0; index < annotations.count; index++){
            PDFAnnotationButtonWidget *widget = annotations[index];

            if ([widget.type isEqualTo: @"Widget"]) {
                NSString *string = widget.fieldName;
                if ([string isEqual:@"name"]) {
                    [nameAnnotations addObject:widget];
                }
            }
        }
    }
    return nameAnnotations;
}

- (NSArray *)addressWidgets {
    NSMutableArray *nameAnnotations = [[NSMutableArray alloc] init];
    for (NSInteger pageCount = 0; pageCount < self.document.pageCount; pageCount++) {
        NSArray *annotations = [[self.document pageAtIndex:pageCount] annotations];
        for (NSInteger index = 0; index < annotations.count; index++){
            PDFAnnotationButtonWidget *widget = annotations[index];
            
            if ([widget.type isEqualTo: @"Widget"]) {
                NSString *string = widget.fieldName;
                if ([string isEqual:@"address"]) {
                    [nameAnnotations addObject:widget];
                }
            }
        }
    }
    return nameAnnotations;
}

- (NSArray *)phoneWidgets {
    NSMutableArray *nameAnnotations = [[NSMutableArray alloc] init];
    for (NSInteger pageCount = 0; pageCount < self.document.pageCount; pageCount++) {
        NSArray *annotations = [[self.document pageAtIndex:pageCount] annotations];
        for (NSInteger index = 0; index < annotations.count; index++){
            PDFAnnotationButtonWidget *widget = annotations[index];
            
            if ([widget.type isEqualTo: @"Widget"]) {
                NSString *string = widget.fieldName;
                if ([string isEqual:@"phone"]) {
                    [nameAnnotations addObject:widget];
                }
            }
        }
    }
    return nameAnnotations;
}

- (NSArray *)dateWidgets {
    NSMutableArray *nameAnnotations = [[NSMutableArray alloc] init];
    for (NSInteger pageCount = 0; pageCount < self.document.pageCount; pageCount++) {
        NSArray *annotations = [[self.document pageAtIndex:pageCount] annotations];
        for (NSInteger index = 0; index < annotations.count; index++){
            PDFAnnotationButtonWidget *widget = annotations[index];
            
            if ([widget.type isEqualTo: @"Widget"]) {
                NSString *string = widget.fieldName;
                if ([string isEqual:@"date"]) {
                    [nameAnnotations addObject:widget];
                }
            }
        }
    }
    return nameAnnotations;
}

- (NSArray *)linkWidgets {
    NSMutableArray *nameAnnotations = [[NSMutableArray alloc] init];
    for (NSInteger pageCount = 0; pageCount < self.document.pageCount; pageCount++) {
        NSArray *annotations = [[self.document pageAtIndex:pageCount] annotations];
        for (NSInteger index = 0; index < annotations.count; index++){
            PDFAnnotationButtonWidget *widget = annotations[index];
            
            if ([widget.type isEqualTo: @"Widget"]) {
                NSString *string = widget.fieldName;
                if ([string isEqual:@"link"]) {
                    [nameAnnotations addObject:widget];
                }
            }
        }
    }
    return nameAnnotations;
}

- (NSArray *)ssnWidgets {
    NSMutableArray *nameAnnotations = [[NSMutableArray alloc] init];
    for (NSInteger pageCount = 0; pageCount < self.document.pageCount; pageCount++) {
        NSArray *annotations = [[self.document pageAtIndex:pageCount] annotations];
        for (NSInteger index = 0; index < annotations.count; index++){
            PDFAnnotationButtonWidget *widget = annotations[index];
            
            if ([widget.type isEqualTo: @"Widget"]) {
                NSString *string = widget.fieldName;
                if ([string isEqual:@"ssn"]) {
                    [nameAnnotations addObject:widget];
                }
            }
        }
    }
    return nameAnnotations;
}

- (void)unredactNamesFromDocument:(Document *)document {
    PDFPage *activePage = self.currentPage;
    for (NSInteger index = 0; index < [self.document pageCount]; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            if ([arrayOfWordsOnPage[index] isTest]) {
                [PdfSelections removeAnnotation:arrayOfWordsOnPage[index] onPage:pageR];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:activePage];
}

- (void)redactNamesFromDocument:(Document *)document {
    PDFPage *activePage = self.currentPage;
    for (NSInteger index = 0; index < [self.document pageCount]; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            if ([arrayOfWordsOnPage[index] isTest] && [PDFView checkAgainstDefiniteNames:[arrayOfWordsOnPage[index] text] and:document]) {
                [PdfSelections drawAnnotation:arrayOfWordsOnPage[index] onPage:pageR];
                [self setNeedsDisplay: YES];
            }
            else if ([arrayOfWordsOnPage[index] isTest] && [arrayOfWordsOnPage[index] textWithoutSymbols].length == 1) {
                [PdfSelections drawAnnotation:arrayOfWordsOnPage[index] onPage:pageR];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:activePage];
}

+ (BOOL)checkAgainstDefiniteNames:(NSString *)nameToCheck and:(Document *)document {
    NSMutableArray *createNameList = [[NSMutableArray alloc]init];
    for (NSInteger index = 0; index < document.pageCount; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        [createNameList addObjectsFromArray: pageAtIndex.definiteNames];
    }
    if ([createNameList containsObject:nameToCheck]) {
        return YES;
    }
    return NO;
}

//could probably return an array of selections and display it as a list - to offer which ones to redact and which not to.
// also could give details of what and how many on what pages - 
- (void)redactDatesFromDocument:(Document *)document {
    PDFPage *activePage = self.currentPage;
    for (NSInteger index = 0; index < [self.document pageCount]; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            if ([arrayOfWordsOnPage[index] isDate]) {
                [self setCurrentSelection:[pageR selectionForRange:[arrayOfWordsOnPage[index] rangeForWords]]];
                [self redactHighlights];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:activePage];
}

- (void)unredactDatesFromDocument:(Document *)document {
    PDFPage *activePage = self.currentPage;
    for (NSInteger index = 0; index < [self.document pageCount]; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            if ([arrayOfWordsOnPage[index] isDate]) {
                [PdfSelections removeAnnotation:arrayOfWordsOnPage[index] onPage:pageR];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:activePage];
}

- (void)redactAddressesFromDocument:(Document *)document {
    PDFPage *activePage = self.currentPage;
    for (NSInteger index = 0; index < [self.document pageCount]; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            if ([arrayOfWordsOnPage[index] isAddress]) {
                [self setCurrentSelection:[pageR selectionForRange:[arrayOfWordsOnPage[index] rangeForWords]]];
                [self redactHighlights];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:activePage];
}

- (void)unredactAddressesFromDocument:(Document *)document {
    PDFPage *activePage = self.currentPage;
    for (NSInteger index = 0; index < [self.document pageCount]; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            if ([arrayOfWordsOnPage[index] isAddress]) {
                [PdfSelections removeAnnotation:arrayOfWordsOnPage[index] onPage:pageR];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:activePage];
}

- (void)redactPhonesFromDocument:(Document *)document {
    PDFPage *activePage = self.currentPage;
    for (NSInteger index = 0; index < [self.document pageCount]; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            if ([arrayOfWordsOnPage[index] isPhoneNumber]) {
                [self setCurrentSelection:[pageR selectionForRange:[arrayOfWordsOnPage[index] rangeForWords]]];
                [self redactHighlights];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:activePage];
}

- (void)unredactPhonesFromDocument:(Document *)document {
    PDFPage *activePage = self.currentPage;
    for (NSInteger index = 0; index < [self.document pageCount]; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            if ([arrayOfWordsOnPage[index] isPhoneNumber]) {
                [PdfSelections removeAnnotation:arrayOfWordsOnPage[index] onPage:pageR];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:activePage];
}

- (void)redactSSNsFromDocument:(Document *)document {
    PDFPage *activePage = self.currentPage;
    for (NSInteger index = 0; index < [self.document pageCount]; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            if ([arrayOfWordsOnPage[index] isSSN]) {
                [self setCurrentSelection:[pageR selectionForRange:[arrayOfWordsOnPage[index] rangeForWords]]];
                [self redactHighlights];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:activePage];
}

- (void)unredactSSNsFromDocument:(Document *)document {
    PDFPage *activePage = self.currentPage;
    for (NSInteger index = 0; index < [self.document pageCount]; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            if ([arrayOfWordsOnPage[index] isSSN]) {
                [PdfSelections removeAnnotation:arrayOfWordsOnPage[index] onPage:pageR];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:activePage];
}

- (void)redactLinksFromDocument:(Document *)document {
    PDFPage *activePage = self.currentPage;
    for (NSInteger index = 0; index < [self.document pageCount]; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            if ([arrayOfWordsOnPage[index] isLink]) {
                [self setCurrentSelection:[pageR selectionForRange:[arrayOfWordsOnPage[index] rangeForWords]]];
                [self redactHighlights];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:activePage];
}

- (void)unredactLinksFromDocument:(Document *)document {
    PDFPage *activePage = self.currentPage;
    for (NSInteger index = 0; index < [self.document pageCount]; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            if ([arrayOfWordsOnPage[index] isLink]) {
                [PdfSelections removeAnnotation:arrayOfWordsOnPage[index] onPage:pageR];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:activePage];
}

- (void)redactCurrentPage {
    PDFSelection *allSelect = [self.document selectionForEntireDocument];
    PDFAnnotationButtonWidget *annotation = [PDFView annotation:@"page" andLocation:[allSelect boundsForPage:self.currentPage]];
    [self toggleAnnotationButtonWidget:annotation];
    [self.currentPage addAnnotation:annotation];
    [self setNeedsDisplay: YES];
}

- (void)unredactSelectedWordsForDocument:(Document *)document {
    PDFPage *currentPage = self.currentPage;
    for (NSInteger index = 0; index < self.document.pageCount; index++) {
        Page *page = [document pageAtIndex:index];
        PDFPage *PDFPage = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = page.words;
        [self goToPage:PDFPage];
        for (NSInteger index = 0; index < [arrayOfWordsOnPage count]; index++) {
            if ([[arrayOfWordsOnPage[index] text] isEqual:self.currentSelection.string]) {
                [PdfSelections removeAnnotation:arrayOfWordsOnPage[index] onPage:PDFPage];
                [self setNeedsDisplay: YES];
            }
            else if ([[[arrayOfWordsOnPage[index] text] stringByReplacingOccurrencesOfString:@"." withString:@"" ] isEqual:self.currentSelection.string]){
                [PdfSelections removeAnnotation:arrayOfWordsOnPage[index] onPage:PDFPage];
                [self setNeedsDisplay: YES];
            }

        }
    }
    [self goToPage:currentPage];
}

- (void)redactHighlights {
    for (NSInteger index = 0; index < [self.currentSelection.pages count]; index++) {
        [self goToPage:self.currentSelection.pages[index]];
        for (PDFSelection *selection in self.currentSelection.selectionsByLine) {
            NSRect frame = [selection boundsForPage:self.currentSelection.pages[index]];
            PDFAnnotationButtonWidget *annotation = [PDFView annotation:@"line" andLocation:frame];
            [self.currentPage addAnnotation:annotation];
            [self toggleAnnotationButtonWidget:annotation];
        }
        [self setNeedsDisplay:YES];
    }
    [self setNeedsDisplay:YES];
    [self goToPage:self.currentSelection.pages[0]];
}

- (void)redactSelectedWordsForDocument:(Document *)document {
    PDFPage *currentPage = self.currentPage;
    for (NSInteger index = 0; index < self.document.pageCount; index++) {
        Page *pageAtIndex = [document pageAtIndex:index];
        PDFPage *pageR = [self.document pageAtIndex:index];
        NSArray *arrayOfWordsOnPage = pageAtIndex.words;
        [self goToPage:pageR];
        for (NSInteger index = 0; index < arrayOfWordsOnPage.count; index++) {
            NSLog(@"the selected word is %@",self.currentSelection.string);
            NSLog(@"the other word is %@",[arrayOfWordsOnPage[index] text]);
            if ([[arrayOfWordsOnPage[index] text] isEqual:self.currentSelection.string]) {
                [PdfSelections drawAnnotation:arrayOfWordsOnPage[index] onPage:pageR];
                [self setNeedsDisplay: YES];
            }
            else if ([[[arrayOfWordsOnPage[index] text] stringByReplacingOccurrencesOfString:@"." withString:@"" ] isEqual:self.currentSelection.string]){
                [PdfSelections drawAnnotation:arrayOfWordsOnPage[index] onPage:pageR];
                [self setNeedsDisplay: YES];
            }
        }
    }
    [self goToPage:currentPage];
}

+ (PDFAnnotationButtonWidget *)annotation:(NSString *)withFieldName andLocation:(NSRect)rect {
    PDFAnnotationButtonWidget *annotation = [[PDFAnnotationButtonWidget alloc] init];
    
    annotation.bounds = rect;
    annotation.backgroundColor = [NSColor clearColor];
    annotation.color = [NSColor clearColor];
    annotation.fieldName = withFieldName;
    annotation.controlType = kPDFWidgetPushButtonControl;
    return annotation;
}

- (void)toggleAnnotationButtonWidget:(PDFAnnotationButtonWidget *)widget {
    if (widget.backgroundColor.alphaComponent > 0){
        widget.backgroundColor = [NSColor clearColor];
    } else {
        widget.backgroundColor = [PdfSelections theOpaqueColor];
    }
    if ([widget.fieldName isEqualToString:@"page"]) {
        [self.currentPage removeAnnotation:widget];
    }
    [self setNeedsDisplay: YES];
}

@end
